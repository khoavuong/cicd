import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Form, Modal } from 'antd';
import { useLoading } from '../../components/LoadingProvider';
import { changePassword, updateInfoUser } from '../../api/user';
import { useUser } from '../../components/UserProvider';
import { alertError, alertSuccess } from '../../utils/alert';
import { FormInstance } from 'antd/lib/form';
import { EditButton, Label, PillInput } from '../../components/CommonStyles';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import { getGoogleApiKey } from '../../utils/getGoogleKey';
import { states } from '../../constants/states';
import { ChangePasswordPayload } from '../../interface/Auth';
interface FormInterface {
  id: string;
  email: string;
  phone: string;
  username: string;
  address: string;
  provinceCode: number;
}

interface ChangePasswordFormInterface extends ChangePasswordPayload {
  reNewPassword: string;
}

export const Account = () => {
  const { user, setUser } = useUser();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [form]: FormInstance<FormInterface>[] = Form.useForm();
  const changePasswordUseForm: FormInstance<ChangePasswordFormInterface>[] = Form.useForm();
  const changePasswordForm = changePasswordUseForm[0];

  const { setLoading } = useLoading();
  const [, forceUpdate] = useState([]);

  useEffect(() => {
    form.setFieldsValue(user);
    forceUpdate([]);
  }, [form, user]);

  const handleOk = async () => {
    const { reNewPassword, newPassword, oldPassword } = changePasswordForm.getFieldsValue();
    if (reNewPassword !== newPassword) {
      alertError('Mật khẩu mới nhập lại không đúng');
      return;
    }
    try {
      setLoading(true);
      await changePassword({ newPassword, oldPassword });
      changePasswordForm.resetFields();
      alertSuccess('Đổi mật khẩu thành công');
      setIsModalVisible(false);
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const onFinish = async () => {
    try {
      setLoading(true);
      const newInfo = form.getFieldsValue();
      await updateInfoUser(newInfo);
      setUser({ ...user, ...newInfo });
      alertSuccess('Cập nhật thông tin thành công');
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const onChangePassClick = () => {
    setIsModalVisible(true);
  };

  const onPlaceSearch = (value: any) => {
    const stateName = value.value.terms[value.value.terms.length - 2].value;
    const findProvinceCode = states.find((state) => state.label === stateName)?.value || 0;

    form.setFieldsValue({
      ...form.getFieldsValue(),
      provinceCode: findProvinceCode,
      address: value.label,
    });

    forceUpdate([]);
  };

  return (
    <Container>
      <Title>Chỉnh sửa thông tin tài khoản</Title>
      <Form initialValues={user} onFinish={onFinish} form={form}>
        <Label>ID</Label>
        <Form.Item name="id">
          <PillInput disabled size="large" />
        </Form.Item>

        <Label>Email</Label>
        <Form.Item name="email" rules={[{ required: true, message: 'Vui lòng nhập email' }]}>
          <PillInput disabled size="large" type="email" />
        </Form.Item>

        <Label>Số điện thoại</Label>
        <Form.Item
          name="phone"
          rules={[{ required: true, message: 'Vui lòng nhập số điện thoại' }]}
        >
          <PillInput disabled size="large" type="tel" />
        </Form.Item>

        <Label>Tên tài khoản</Label>
        <Form.Item
          name="username"
          rules={[{ required: true, message: 'Vui lòng nhập tên tài khoản' }]}
        >
          <PillInput size="large" type="text" />
        </Form.Item>

        <Label>Địa chỉ</Label>
        <Form.Item name="address" rules={[{ required: true, message: 'Vui lòng nhập Địa chỉ' }]}>
          <GooglePlacesAutocomplete
            apiOptions={{ language: 'vi', region: 'vi' }}
            apiKey={getGoogleApiKey()}
            selectProps={{
              value: {
                label: form.getFieldValue('address') || '',
                value: form.getFieldValue('address') || '',
              },
              onChange: onPlaceSearch,
              styles: {
                control: (provided: any) => ({
                  ...provided,
                  borderRadius: '20px',
                }),
              },
            }}
          />
        </Form.Item>

        <Form.Item style={{ display: 'none' }} name="provinceCode">
          <PillInput />
        </Form.Item>

        <Label>Mật khẩu</Label>
        <Form.Item>
          <PillInput value="password" disabled size="large" type="password" />
        </Form.Item>

        <ChangePassword onClick={onChangePassClick}>Đổi mật khẩu</ChangePassword>

        <Form.Item>
          <EditButton htmlType="submit" size="large" type="primary">
            Cập nhật
          </EditButton>
        </Form.Item>
      </Form>

      <Modal
        title="Đổi mật khẩu đăng nhập"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Hoàn thành"
      >
        <Form name="change_password" form={changePasswordForm}>
          <Label>Mật khẩu cũ</Label>
          <Form.Item name="oldPassword">
            <PillInput size="large" type="password" />
          </Form.Item>

          <Label>Mật khẩu mới</Label>
          <Form.Item name="newPassword">
            <PillInput size="large" type="password" />
          </Form.Item>

          <Label>Nhập lại mật khẩu mới</Label>
          <Form.Item name="reNewPassword">
            <PillInput size="large" type="password" />
          </Form.Item>
        </Form>
      </Modal>
    </Container>
  );
};

const Container = styled.div`
  width: 500px;
  margin: auto;
  margin-top: 2rem;
`;

const Title = styled.h1`
  font-weight: bold;
  text-align: center;
`;

const ChangePassword = styled.span`
  cursor: pointer;
  color: #00467f;
  font-weight: bold;
`;
