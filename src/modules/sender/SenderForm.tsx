import React, { useState } from 'react';
import { Form, Divider, Checkbox, Layout, Modal, Tooltip, Row, Col } from 'antd';
import styled from 'styled-components';
import { useHistory } from 'react-router';
import { FormInstance } from 'antd/lib/form';
import { getAccessToken } from '../../utils/accessToken';
import { createOrder } from '../../api/order';
import { useLoading } from '../../components/LoadingProvider';
import { useUser } from '../../components/UserProvider';
import { alertError } from '../../utils/alert';
import {
  InfoContainer,
  InfoTitle,
  PillButton,
  PillInput,
  PillInputNumber,
  PillSelect,
} from '../../components/CommonStyles';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import { getGoogleApiKey } from '../../utils/getGoogleKey';
import { states } from '../../constants/states';
import { getTransportationCost } from '../../api/user';
import { PaymentSide } from '../../interface/Order';
import { QuestionCircleOutlined } from '@ant-design/icons';
import ReactGA from 'react-ga';

interface FormInterface {
  paymentSide: PaymentSide;
  phoneReceiver: string;
  nameReceiver: string;
  emailReceiver: string;
  kind: string;
  weight: number;
  value: number;
  description: string;
  receiverId: string;
  provinceCode: number;
  toAddress: string;
  isDirectlySend: number;
  isDirectlyReceive: number;
}

export const SenderForm = ({ isEditMode = false }) => {
  const { user } = useUser();
  const [form]: FormInstance<FormInterface>[] = Form.useForm();
  const history = useHistory();
  const { setLoading } = useLoading();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [fee, setFee] = useState(0);
  const [destStockId, setDestStockId] = useState(null);
  const [srcStockId, setSrcStockId] = useState(null);
  const [addressDestWarehouse, setAddressDestWarehouse] = useState('');
  const [addressSrcWarehouse, setAddressSrcWarehouse] = useState('');

  const {
    toAddress,
    phoneReceiver,
    nameReceiver,
    emailReceiver,
    kind,
    weight,
    description,
    isDirectlySend,
    isDirectlyReceive,
    value,
  } = form.getFieldsValue();

  const [, forceUpdate] = useState([]);

  const onFinish = async () => {
    setIsModalVisible(true);
    const { weight, toAddress, isDirectlyReceive, isDirectlySend, provinceCode } =
      form.getFieldsValue();

    const payload = {
      isDirectlySend: isDirectlySend ? 1 : 0,
      isDirectlyReceive: isDirectlyReceive ? 1 : 0,
      addressReceiver: toAddress,
      addressReceiverCode: provinceCode,
      weight: weight,
    };

    try {
      setLoading(true);
      const { cost, destStockId, srcStockId, addressDestWarehouse, addressSrcWarehouse } =
        await getTransportationCost(payload);

      setFee(parseInt(cost.split(',').join('')));
      setDestStockId(destStockId);
      setSrcStockId(srcStockId);
      setAddressDestWarehouse(addressDestWarehouse);
      setAddressSrcWarehouse(addressSrcWarehouse);
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const deleteOrder = () => {
    history.push(`/sender/orders`);
  };

  const handleOk = async () => {
    const orderData = {
      ...form.getFieldsValue(),
      isDirectlyReceive: form.getFieldsValue().isDirectlyReceive ? 1 : 0,
      isDirectlySend: form.getFieldsValue().isDirectlySend ? 1 : 0,
      emailSender: user.email,
      nameSender: user.username,
      phoneSender: user.phone,
      fromAddress: user.address,
      senderId: user.id,
      fee,
      srcStockId,
      destStockId,
      addressDestWarehouse,
      addressSrcWarehouse,
      reqTime: Date.now(),
      accessToken: getAccessToken(),
    };

    try {
      setLoading(true);
      ReactGA.event({
        category: 'User',
        action: 'User create order',
      });
      await createOrder({ ...orderData });
      setLoading(false);
      history.push(`/sender/orders`);
    } catch (err) {
      setLoading(false);
      alertError(err.message);
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const onPlaceSearch = (value: any) => {
    const stateName = value.value.terms[value.value.terms.length - 2].value;
    const findProvinceCode = states.find((state) => state.label === stateName)?.value || 0;

    form.setFieldsValue({
      ...form.getFieldsValue(),
      provinceCode: findProvinceCode,
      toAddress: value.label,
    });

    forceUpdate([]);
  };

  const onPlaceFocus = () => {
    form.setFieldsValue({
      ...form.getFieldsValue(),
      toAddress: '',
    });
    forceUpdate([]);
  };

  return (
    <Form
      {...layout}
      form={form}
      name="control-hooks"
      onFinish={onFinish}
      initialValues={{
        ...form.getFieldsValue(),
        isDirectlySend: 0,
        isDirectlyReceive: 0,
      }}
    >
      <Layout>
        <Layout style={{ marginRight: '250px' }}>
          <CustomContent>
            <Title>Bên gửi</Title>
            <Form.Item label="Số điện thoại" rules={[{ required: true }]}>
              <PillInput value={user?.phone} disabled />
            </Form.Item>
            <Form.Item label="Họ tên" rules={[{ required: true }]}>
              <PillInput value={user?.username} disabled />
            </Form.Item>
            <Form.Item label="Địa chỉ">
              <PillInput value={user?.address} disabled />
            </Form.Item>
            <Form.Item label="Email">
              <PillInput value={user?.email} disabled />
            </Form.Item>

            <Form.Item {...tailLayout} name="isDirectlySend" valuePropName="checked">
              <Checkbox>Gửi hàng trực tiếp tại kho gửi</Checkbox>
            </Form.Item>

            <Form.Item {...tailLayout} name="isDirectlyReceive" valuePropName="checked">
              <Checkbox>Nhận hàng trực tiếp tại kho đến</Checkbox>
            </Form.Item>
            <Divider style={{ backgroundColor: 'red' }} />

            <Title>Bên nhận</Title>

            <Form.Item name="phoneReceiver" label="Số điện thoại" rules={requiredFieldMessage}>
              <PillInput />
            </Form.Item>
            <Form.Item name="nameReceiver" label="Họ tên" rules={requiredFieldMessage}>
              <PillInput />
            </Form.Item>
            <Form.Item
              name="emailReceiver"
              label="Email"
              rules={[
                {
                  type: 'email',
                  message: 'Không đúng định dạng mail',
                  required: true,
                },
              ]}
            >
              <PillInput />
            </Form.Item>

            <Form.Item name="toAddress" label="Địa chỉ" rules={requiredFieldMessage}>
              <GooglePlacesAutocomplete
                apiOptions={{ language: 'vi', region: 'vi' }}
                apiKey={getGoogleApiKey()}
                selectProps={{
                  value: {
                    label: toAddress || '',
                    value: toAddress || '',
                  },
                  onChange: onPlaceSearch,
                  onFocus: onPlaceFocus,
                  styles: {
                    control: (provided: any) => ({
                      ...provided,
                      borderRadius: '20px',
                    }),
                  },
                }}
              />
            </Form.Item>

            <Form.Item name="provinceCode" label="Tỉnh thành" rules={requiredFieldMessage}>
              <PillSelect disabled>
                {states.map((state) => (
                  <Option key={state.value} value={state.value}>
                    {state.label}
                  </Option>
                ))}
              </PillSelect>
            </Form.Item>

            <Divider style={{ backgroundColor: 'red' }} />

            <Title>Hàng hóa</Title>
            <Form.Item name="kind" label="Loại hàng" rules={requiredFieldMessage}>
              <PillInput />
            </Form.Item>
            <Form.Item name="weight" label="Khối lượng ước tính (kg)" rules={requiredFieldMessage}>
              <PillInputNumber
                style={{ width: '100%' }}
                formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              />
            </Form.Item>
            <Form.Item
              name="value"
              label={
                <Tooltip title="Giá trị ước tính của hàng hóa để đền bù khi gặp sự cố">
                  Giá trị hàng hóa <QuestionCircleOutlined />
                </Tooltip>
              }
              rules={requiredFieldMessage}
            >
              <PillInputNumber
                style={{ width: '100%' }}
                formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              />
            </Form.Item>
          </CustomContent>
        </Layout>

        <StickySider>
          <div style={{ padding: 10 }}>
            <p>Tuỳ chọn thanh toán</p>
            <Form.Item wrapperCol={{ span: 20 }} name="paymentSide" rules={requiredFieldMessage}>
              <PillSelect placeholder="Chọn bên trả phí">
                <Option value="sender">Bên gửi trả phí</Option>
                <Option value="receiver">Bên nhận trả phí</Option>
              </PillSelect>
            </Form.Item>

            <Form.Item wrapperCol={{ span: 20 }} name="description">
              <TextArea rows={4} placeholder="Ghi chú" />
            </Form.Item>

            <Form.Item>
              <PillButton type="primary" htmlType="submit">
                {isEditMode ? 'Cập nhật đơn' : 'Tạo đơn'}
              </PillButton>
            </Form.Item>
            {isEditMode && (
              <PillButton type="primary" danger onClick={deleteOrder}>
                Hủy đơn
              </PillButton>
            )}
          </div>
        </StickySider>
      </Layout>

      <Modal
        title={isEditMode ? 'Xác nhận cập nhật đơn hàng' : 'Xác nhận tạo đơn hàng'}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Xác nhận"
        width={600}
      >
        <div>
          <InfoTitle>| Bên nhận</InfoTitle>
          <InfoContainer>
            <p>
              Số điện thoại: <strong>{phoneReceiver}</strong>
            </p>
            <p>
              Họ tên: <strong>{nameReceiver}</strong>
            </p>
            <p>
              Email: <strong>{emailReceiver}</strong>
            </p>
            <p>
              Địa chỉ: <strong>{toAddress}</strong>
            </p>
          </InfoContainer>
        </div>

        <br />

        <Row>
          <Col span={12}>
            <InfoTitle>| Hàng hoá</InfoTitle>
            <InfoContainer>
              <p>
                Loại hàng: <strong>{kind}</strong>
              </p>
              <p>
                Khối lượng ước tính (kg): <strong>{weight?.toLocaleString()}</strong>
              </p>
              <p>
                Giá trị ước tính (vnd): <strong>{value?.toLocaleString()}</strong>
              </p>
            </InfoContainer>
          </Col>
          <Col span={12}>
            <InfoTitle>| Tuỳ chọn thanh toán</InfoTitle>
            <InfoContainer>
              <strong>Bên gửi trả phí</strong>
            </InfoContainer>
          </Col>
        </Row>

        <br />

        <Row gutter={16}>
          <Col span={12}>
            <InfoTitle>| Ghi chú</InfoTitle>
            <InfoContainer>
              <p>
                <strong>{description}</strong>
              </p>
            </InfoContainer>
          </Col>

          <Col span={12}>
            <InfoTitle>| Cách thức vận chuyển hàng</InfoTitle>
            <InfoContainer>
              <p>
                Chiều gửi:{' '}
                <strong>
                  {isDirectlySend
                    ? 'Gửi hàng trực tiếp tại kho gửi'
                    : 'Xe giao vận đến lấy hàng về kho'}
                </strong>
              </p>
              <p>
                Chiều nhận:{' '}
                <strong>
                  {isDirectlyReceive
                    ? 'Nhận hàng trực tiếp tại kho đến'
                    : 'Xe giao vận đi gửi hàng'}
                </strong>
              </p>
            </InfoContainer>
          </Col>
        </Row>

        <SubTitle>Tổng phí</SubTitle>
        <Price>{fee?.toLocaleString()} VND</Price>
      </Modal>
    </Form>
  );
};

const { Option } = PillSelect;
const { Sider, Content } = Layout;
const { TextArea } = PillInput;

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 8,
  },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const requiredFieldMessage = [{ required: true, message: 'This field is required!' }];

const StickySider = styled(Sider)`
  overflow: auto;
  height: 100%;
  position: fixed;
  right: 0;
  background-color: white;
  box-shadow: 0 4px 10px rgb(0 0 0 / 20%);
  max-width: 270px !important;
  width: 270px !important;
`;

const CustomContent = styled(Content)`
  background-color: white;
  padding: 2rem;
`;

const Title = styled.div`
  text-align: center;
  color: #f26522;
  font-weight: 600;
  font-size: 16px;
  margin-bottom: 10px;
`;

const SubTitle = styled.p`
  color: #00467f;
  font-size: 12px;
  margin-bottom: 4px;
  margin-top: 2rem;
  line-height: 13px;
`;

const Price = styled.p`
  color: red;
  font-size: 22px;
`;
