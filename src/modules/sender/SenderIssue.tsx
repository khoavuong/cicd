import React, { useEffect, useState, ChangeEventHandler } from 'react';
import { Table, Form, Modal, Select } from 'antd';
import styled from 'styled-components';
import { EditOutlined } from '@ant-design/icons';
import {
  BlueText,
  FilterWrapper,
  FlexCenter,
  Label,
  PillButton,
  PillDatePicker,
  PillInput,
  PillSelect,
} from '../../components/CommonStyles';
import TextArea from 'antd/lib/input/TextArea';
import { FormInstance } from 'antd/lib/form';
import {
  SubmitSupportRequestPayload,
  SupportRequest,
} from '../../interface/Order';
import { useUser } from '../../components/UserProvider';
import { alertError, alertSuccess } from '../../utils/alert';
import { useLoading } from '../../components/LoadingProvider';
import { CalendarOutlined, FieldNumberOutlined } from '@ant-design/icons';
import {
  getOrderByUser,
  getSupportRequestByUser,
  removeSupportRequest,
  submitSupportRequest,
} from '../../api/order';
import { getAccessToken } from '../../utils/accessToken';
import { EmptyTableContent } from '../../components/EmptyTable';
import { endOfDay, startOfDay } from 'date-fns';
// import { issueRequests } from '../../constants/mockTableData';

interface RequestsTable extends SupportRequest {
  key: string;
}

export const SenderIssue = () => {
  const { user } = useUser();
  const { setLoading } = useLoading();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [ordersSelect, setOrdersSelect] = useState([]);
  const [requests, setRequests] = useState<RequestsTable[]>([]);
  const [form]: FormInstance<SubmitSupportRequestPayload>[] = Form.useForm();
  const [orderFilter, setOrderFilter] = useState('');
  const [fromDay, setFromDay] = useState(0);
  const [toDay, setToDay] = useState(Infinity);

  const filterRequestData = requests
    .filter((data) => {
      if (orderFilter === '') return true;
      return `${data.orderId}`
        .toLowerCase()
        .includes(orderFilter.toLowerCase());
    })
    .filter((data) => {
      return data.reqTime >= fromDay;
    })
    .filter((data) => {
      return data.reqTime <= toDay;
    });

  useEffect(() => {
    const callGetOrderByUser = async () => {
      const payload = {
        userId: user.id,
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };

      try {
        setLoading(true);
        const { orders } = await getOrderByUser(payload);
        const mappedOrdersSelect = orders.map((order) => order.orderId);
        setOrdersSelect(mappedOrdersSelect);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    if (user) callGetOrderByUser();
  }, [setLoading, user]);

  useEffect(() => {
    const callGetSupportRequestByUser = async () => {
      const payload = {
        userId: user.id,
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };
      try {
        setLoading(true);
        const res = await getSupportRequestByUser(payload);
        const { supportRequests } = res;
        const mappedSupportRequest = supportRequests.map((item) => ({
          ...item,
          key: item.orderId + item.content,
        }));
        setRequests(mappedSupportRequest);

        // setRequests(issueRequests);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };
    if (user?.id) callGetSupportRequestByUser();
  }, [setLoading, user?.id]);

  const handleOk = async () => {
    const payload = {
      ...form.getFieldsValue(),
      reqTime: Date.now(),
      accessToken: getAccessToken(),
    };
    try {
      setLoading(true);
      await submitSupportRequest(payload);
      alertSuccess('Tạo yêu cầu thành công');
      const { orderId, content } = form.getFieldsValue();
      const newRequest: RequestsTable[] = [
        ...requests,
        {
          key: orderId + content,
          content,
          reqTime: Date.now(),
          orderId,
          userId: user.id,
          reply: '',
        },
      ];
      setRequests(newRequest);
    } catch (err) {
      alertError(err.message);
    } finally {
      form.resetFields();
      setLoading(false);
      setIsModalVisible(false);
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const onClick = () => {
    setIsModalVisible(true);
    form.setFieldsValue({
      ...form.getFieldsValue(),
      userId: user?.id,
      emailAddress: user?.email,
    });
  };

  const onDeleteClick = (request: RequestsTable) => {
    const onDeleteOk = async () => {
      const payload = {
        reqTime: Date.now(),
        accessToken: getAccessToken(),
        orderId: request.orderId,
        userId: user.id,
      };
      try {
        await removeSupportRequest(payload);
        alertSuccess('Xóa yêu cầu thành công');
        setRequests(
          requests.filter((item) => item.orderId !== request.orderId)
        );
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    Modal.warning({
      title: 'Bạn có chắc muốn xóa yêu cầu?',
      closable: true,
      okText: 'Có',
      okButtonProps: { danger: true },
      onOk: onDeleteOk,
    });
  };

  const columns = [
    {
      title: 'Mã đơn hàng',
      render: (_: any, request: RequestsTable) => {
        return (
          <BlueText>
            <p>
              <FieldNumberOutlined /> {request.orderId}
            </p>
            <p>
              <CalendarOutlined /> {new Date(request.reqTime).toLocaleString()}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Nội dung',
      render: (_: any, request: RequestsTable) => {
        return <BlueText>{request.content}</BlueText>;
      },
    },
    {
      title: 'Tình trạng phản hồi',
      render: (_: any, request: RequestsTable) => {
        return <BlueText>{request.reply || 'Đang giải quyết'} </BlueText>;
      },
    },
    {
      title: 'Xóa yêu cầu',
      render: (_: any, request: RequestsTable) => {
        return (
          <PillButton
            onClick={() => onDeleteClick(request)}
            danger
            type="primary"
          >
            Delete
          </PillButton>
        );
      },
    },
  ];

  const onOrderFilterChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setOrderFilter(e.target.value);
  };

  const onFromDayChange = (date: any) => {
    setFromDay(date ? startOfDay(date.valueOf()).getTime() : 0);
  };

  const onToDayChange = (date: any) => {
    setToDay(date ? endOfDay(date.valueOf()).getTime() : Infinity);
  };

  return (
    <Container>
      <PillButton type="primary" onClick={onClick}>
        <EditOutlined />
        Lên yêu cầu
      </PillButton>
      <br />
      <br />
      <FlexCenter>
        <FilterWrapper>
          <Label>Mã yêu cầu</Label>
          <PillInput
            onChange={onOrderFilterChange}
            placeholder="Lọc theo mã yêu cầu"
            value={orderFilter}
          />
        </FilterWrapper>

        <FilterWrapper>
          <Label>Từ đầu ngày</Label>
          <PillDatePicker onChange={onFromDayChange} />
        </FilterWrapper>

        <FilterWrapper>
          <Label>Đến cuối ngày</Label>
          <PillDatePicker onChange={onToDayChange} />
        </FilterWrapper>
      </FlexCenter>
      <br />
      <Table
        dataSource={filterRequestData}
        bordered
        columns={columns}
        pagination={{ hideOnSinglePage: true }}
        locale={{ emptyText: <EmptyTableContent /> }}
      />

      <Modal
        title="Gửi yêu cầu hỗ trợ"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Xác nhận"
      >
        <Form form={form} name="normal_login">
          <Label>User ID</Label>
          <Form.Item name="userId">
            <PillInput disabled />
          </Form.Item>

          <Label>Email</Label>
          <Form.Item name="emailAddress">
            <PillInput disabled />
          </Form.Item>

          <Label>Mã đơn hàng</Label>
          <Form.Item name="orderId">
            <PillSelect placeholder="Chọn yêu cầu">
              {ordersSelect.map((item) => (
                <Option key={item} value={item}>
                  {item}
                </Option>
              ))}
            </PillSelect>
          </Form.Item>

          <Label>Nội dung</Label>
          <Form.Item name="content">
            <TextArea rows={4} placeholder="Nhập nội dung" />
          </Form.Item>
        </Form>
      </Modal>
    </Container>
  );
};

const Container = styled.div`
  margin: auto;
  max-width: 90%;
  margin-bottom: 100px;
  margin-top: 50px;
`;

const { Option } = Select;
