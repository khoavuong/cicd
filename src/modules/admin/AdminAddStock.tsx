import { Form } from 'antd';
import { FormInstance } from 'antd/lib/form';
import React, { useState } from 'react';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import styled from 'styled-components';
import { addAddressWarehouse } from '../../api/user';
import { EditButton, Label } from '../../components/CommonStyles';
import { useLoading } from '../../components/LoadingProvider';
import { states } from '../../constants/states';
import { alertError, alertSuccess } from '../../utils/alert';
import { getGoogleApiKey } from '../../utils/getGoogleKey';

interface AddAddressForm {
  areaCode: number;
  address: string;
}

export const AdminAddStock = () => {
  const [form]: FormInstance<AddAddressForm>[] = Form.useForm();
  const { setLoading } = useLoading();
  const [, forceUpdate] = useState([]);

  const onFinish = async () => {
    try {
      setLoading(true);
      await addAddressWarehouse(form.getFieldsValue());
      form.resetFields();
      alertSuccess('Thêm kho thành công');
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const onPlaceSearch = (value: any) => {
    const stateName = value.value.terms[value.value.terms.length - 2].value;
    const findAreaCode =
      states.find((state) => state.label === stateName)?.value || 0;

    form.setFieldsValue({
      areaCode: findAreaCode,
      address: value.label,
    });

    forceUpdate([]);
  };

  return (
    <Container>
      <Title>THÊM KHO</Title>
      <Form name="normal_login" onFinish={onFinish} form={form}>
        <Label>Địa chỉ</Label>
        <Form.Item
          name="address"
          rules={[{ required: true, message: 'Vui lòng nhập Địa chỉ' }]}
        >
          <GooglePlacesAutocomplete
            apiOptions={{ language: 'vi', region: 'vi' }}
            apiKey={getGoogleApiKey()}
            selectProps={{
              value: {
                label: form.getFieldValue('address') || '',
                value: form.getFieldValue('address') || '',
              },
              onChange: onPlaceSearch,
              styles: {
                control: (provided: any) => ({
                  ...provided,
                  borderRadius: '20px',
                }),
              },
            }}
          />
        </Form.Item>

        <Form.Item style={{ display: 'none' }} name="areaCode" />

        <Form.Item>
          <EditButton size="large" type="primary" htmlType="submit">
            Thêm kho
          </EditButton>
        </Form.Item>
      </Form>
    </Container>
  );
};

const Container = styled.div`
  width: 500px;
  margin: auto;
  margin-top: 1rem;
`;

const Title = styled.h1`
  font-weight: bold;
  text-align: center;
`;
