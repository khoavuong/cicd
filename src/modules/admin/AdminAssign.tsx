import React, { ChangeEventHandler, useEffect, useState } from 'react';
import { Table, Form, Tabs, Select } from 'antd';
import styled from 'styled-components';
import Modal from 'antd/lib/modal/Modal';
import { useLoading } from '../../components/LoadingProvider';
import { alertError } from '../../utils/alert';
import { getListDriver, getListStocker } from '../../api/user';
import {
  DriverRoleText,
  GetListDriverResponse,
  GetListStockerResponse,
  Vehicle,
} from '../../interface/Auth';
import {
  BlueText,
  FilterWrapper,
  FlexCenter,
  Label,
  PillButton,
  PillInput,
} from '../../components/CommonStyles';
import { useHistory } from 'react-router';
import { useUnusedVehicles } from '../../hooks/useUnusedVehicles';
import {
  MailOutlined,
  UserOutlined,
  HomeOutlined,
  FieldNumberOutlined,
} from '@ant-design/icons';
import { ReactComponent as WeightIcon } from '../../assets/weight.svg';

export const AdminAssign = () => {
  const { unusedVehicles } = useUnusedVehicles();
  const { setLoading } = useLoading();
  const history = useHistory();

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [drivers, setDrivers] = useState<GetListDriverResponse[]>([]);
  const [stockKeepers, setStockKeepers] = useState<GetListStockerResponse[]>(
    []
  );

  const [driverIdFilter, setDriverIdFilter] = useState('');
  const [driverWarehouseIdFilter, setDriverWarehouseIdFilter] = useState('');
  const [driverRoleFilter, setDriverRoleFilter] = useState('None');

  const filterDrivers = drivers
    .filter((driver) => {
      if (driverIdFilter === '') return true;
      return `${driver.id}`
        .toLowerCase()
        .includes(driverIdFilter.toLowerCase());
    })
    .filter((driver) => {
      if (driverWarehouseIdFilter === '') return true;
      return `${driver.warehouseResponse.id}`
        .toLowerCase()
        .includes(driverWarehouseIdFilter.toLowerCase());
    })
    .filter((driver) => {
      if (driverRoleFilter === 'None') return true;
      return DriverRoleText[driver.role] === driverRoleFilter;
    });

  const filterStockKeepers = stockKeepers.filter((stockKeeper) => {
    if (driverWarehouseIdFilter === '') return true;
    return `${stockKeeper.warehouseResponse.id}`
      .toLowerCase()
      .includes(driverWarehouseIdFilter.toLowerCase());
  });

  const onDriverIdFilterChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setDriverIdFilter(e.target.value);
  };

  const onDriverWarehouseIdFilterChange: ChangeEventHandler<HTMLInputElement> =
    (e) => {
      setDriverWarehouseIdFilter(e.target.value);
    };

  const onDriverRoleFilterChange = (value: any) => {
    setDriverRoleFilter(value);
  };

  useEffect(() => {
    const callGetListDriver = async () => {
      try {
        setLoading(true);
        const res = await getListDriver();
        const driversWithTableKey = res.map((driver) => ({
          ...driver,
          key: driver.id,
        }));
        setDrivers(driversWithTableKey);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    callGetListDriver();
  }, [setLoading]);

  useEffect(() => {
    const callGetListStockkeeper = async () => {
      try {
        setLoading(true);
        const res = await getListStocker();
        const stockKeepersWithTableKey = res.map((stockKeeper) => ({
          ...stockKeeper,
          key: stockKeeper.id,
        }));
        setStockKeepers(stockKeepersWithTableKey);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    callGetListStockkeeper();
  }, [setLoading]);

  const handleOk = async () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const onClick = () => {
    setIsModalVisible(true);
  };

  const onTabChange = (key: any) => {
    history.push(key);
  };

  return (
    <Container>
      <PillButton type="primary" onClick={onClick}>
        Gán tài xế và phương tiện
      </PillButton>
      <br />
      <br />

      <Tabs
        defaultActiveKey={history.location.hash}
        centered
        onChange={onTabChange}
      >
        <TabPane tab="List tài xế" key="#1">
          <FlexCenter>
            <FilterWrapper>
              <Label>ID tài xế</Label>
              <PillInput
                onChange={onDriverIdFilterChange}
                placeholder="Lọc theo ID tài xế"
                value={driverIdFilter}
              />
            </FilterWrapper>

            <FilterWrapper>
              <Label>ID kho</Label>
              <PillInput
                onChange={onDriverWarehouseIdFilterChange}
                placeholder="Lọc theo ID kho"
                value={driverWarehouseIdFilter}
              />
            </FilterWrapper>

            <FilterWrapper>
              <Label>Vai trò</Label>
              <Select
                style={{ width: '100%' }}
                defaultValue="None"
                onChange={onDriverRoleFilterChange}
              >
                <Option value="None">None</Option>
                <Option value={DriverRoleText.ROLE_DRIVER_EXTER}>
                  {DriverRoleText.ROLE_DRIVER_EXTER}
                </Option>
                <Option value={DriverRoleText.ROLE_DRIVER_INTER}>
                  {DriverRoleText.ROLE_DRIVER_INTER}
                </Option>
              </Select>
            </FilterWrapper>
          </FlexCenter>
          <br />
          <Table
            bordered
            columns={requestColumns}
            dataSource={filterDrivers}
            pagination={{ pageSize: 3, hideOnSinglePage: true }}
          />
        </TabPane>
        <TabPane tab="List phương tiện chưa gán" key="#2">
          <Table
            bordered
            columns={requestColumns2}
            dataSource={unusedVehicles}
            pagination={{ pageSize: 3, hideOnSinglePage: true }}
          />
        </TabPane>
        <TabPane tab="List thủ kho" key="#3">
          <FlexCenter>
            <FilterWrapper>
              <Label>ID kho</Label>
              <PillInput
                onChange={onDriverWarehouseIdFilterChange}
                placeholder="Lọc theo ID kho"
                value={driverWarehouseIdFilter}
              />
            </FilterWrapper>
          </FlexCenter>
          <br />
          <Table
            bordered
            columns={requestColumns3}
            dataSource={filterStockKeepers}
          />
        </TabPane>
      </Tabs>

      <Modal
        title="Gán tài xế và phương tiện"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Xác nhận"
      >
        <Form.Item label="ID tài xế">
          <PillInput placeholder="Nhập ID tài xế" />
        </Form.Item>

        <Form.Item label="ID phương tiện">
          <PillInput placeholder="Nhập ID phương tiện" />
        </Form.Item>
      </Modal>
    </Container>
  );
};

const Container = styled.div`
  margin: auto;
  max-width: 90%;
  margin-bottom: 100px;
  margin-top: 50px;
`;

const { TabPane } = Tabs;

const requestColumns = [
  {
    title: 'Tài xế',
    render: (_: any, driver: GetListDriverResponse) => {
      const { id, username, role, email } = driver;
      return (
        <BlueText>
          <p>
            <FieldNumberOutlined /> {id}
          </p>
          <p>
            <UserOutlined /> {username}
          </p>
          <p>
            <MailOutlined /> {email}
          </p>
          <p>Role: {DriverRoleText[role]}</p>
        </BlueText>
      );
    },
  },
  {
    title: 'Phương tiện',
    render: (_: any, driver: GetListDriverResponse) => {
      const { vehicleResponse } = driver;
      return (
        <BlueText>
          <p>
            <FieldNumberOutlined /> {vehicleResponse.id}
          </p>
          <p>
            <WeightIcon /> {vehicleResponse.currentWeight} /{' '}
            {vehicleResponse.totalWeight}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Kho hoạt động',
    render: (_: any, driver: GetListDriverResponse) => {
      const { warehouseResponse } = driver;
      return (
        <BlueText>
          <p>
            <FieldNumberOutlined /> {warehouseResponse.id}
          </p>
          <p>
            <HomeOutlined /> {warehouseResponse.address}
          </p>
        </BlueText>
      );
    },
  },
];

const requestColumns2 = [
  {
    title: 'Phương tiện',
    render: (_: any, vehicle: Vehicle) => {
      const { id, currentWeight, totalWeight } = vehicle;
      return (
        <BlueText>
          <p>
            <FieldNumberOutlined /> {id}
          </p>
          <p>
            <WeightIcon /> {currentWeight} / {totalWeight}
          </p>
        </BlueText>
      );
    },
  },
];

const requestColumns3 = [
  {
    title: 'Thủ kho',
    render: (_: any, stockkeeper: GetListStockerResponse) => {
      const { id, email, username } = stockkeeper;
      return (
        <BlueText>
          <p>
            <FieldNumberOutlined /> {id}
          </p>
          <p>
            <UserOutlined /> {username}
          </p>
          <p>
            <MailOutlined /> {email}
          </p>
        </BlueText>
      );
    },
  },

  {
    title: 'Kho hoạt động',
    render: (_: any, stockkeeper: GetListStockerResponse) => {
      const { warehouseResponse } = stockkeeper;
      return (
        <BlueText>
          <p>
            <FieldNumberOutlined /> {warehouseResponse.id}
          </p>
          <p>
            <HomeOutlined /> {warehouseResponse.address}
          </p>
        </BlueText>
      );
    },
  },
];

const { Option } = Select;
