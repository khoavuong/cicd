/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from 'react';
import { Table, Form, Modal, Tabs } from 'antd';
import styled from 'styled-components';
import { BlueText, Label, PillInput, PillSelect } from '../../components/CommonStyles';
import TextArea from 'antd/lib/input/TextArea';
import { FormInstance } from 'antd/lib/form';
import {
  DetailOrder,
  ReplySupportRequestPayload,
  SubOrderResponse,
  SuborderStatus,
  SuborderStatusText,
  SupportRequest,
} from '../../interface/Order';
import { useUser } from '../../components/UserProvider';
import { alertError, alertSuccess } from '../../utils/alert';
import { useLoading } from '../../components/LoadingProvider';
import {
  getSuborderIssue,
  getSupportRequestByAdmin,
  replySupportRequest,
  updateSubOrderStatus,
} from '../../api/order';
import { getAccessToken } from '../../utils/accessToken';
import { useHistory } from 'react-router';
import { EmptyTableContent } from '../../components/EmptyTable';
// import { issueRequests } from '../../constants/mockTableData';
import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  CalendarOutlined,
  CodeSandboxOutlined,
  FieldNumberOutlined,
} from '@ant-design/icons';
import { ReactComponent as WeightIcon } from '../../assets/weight.svg';
// import { mockSubOrderResponses, mockSupportRequest } from '../../constants/mockTableData';
interface RequestsTable extends SupportRequest {
  key: string;
}

export const AdminIssue = () => {
  const history = useHistory();
  const { user } = useUser();
  const { setLoading } = useLoading();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isStatusModalVisible, setIsStatusModalVisible] = useState(false);
  const [requests, setRequests] = useState<RequestsTable[]>([]);
  const [driverIssues, setDriverIssues] = useState<SubOrderResponse[]>([]);
  const [selectedIssue, setSelectedIssue] = useState<SubOrderResponse>(null);
  const [newStatus, setNewStatus] = useState<SuborderStatus>(null);
  const [form]: FormInstance<ReplySupportRequestPayload>[] = Form.useForm();

  const doneRequests = requests.filter((request) => !!request.reply);
  const inProgressRequests = requests.filter((request) => !request.reply);
  // const inProgressRequests: RequestsTable[] = mockSupportRequest;
  // const doneRequests: RequestsTable[] = mockSupportRequest;

  useEffect(() => {
    const callGetSupportRequestByAdmin = async () => {
      const payload = {
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };
      try {
        setLoading(true);
        const res = await getSupportRequestByAdmin(payload);
        const { supportRequests } = res;
        const mappedSupportRequest = supportRequests.map((item) => ({
          ...item,
          key: item.orderId + item.content,
        }));
        setRequests(mappedSupportRequest);
        // setRequests(issueRequests);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };
    if (user?.id) callGetSupportRequestByAdmin();
  }, [setLoading, user?.id]);

  useEffect(() => {
    const callGetSuborderIssue = async () => {
      const payload = {
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };
      try {
        setLoading(true);
        const { subOrderResponses } = await getSuborderIssue(payload);
        const mappedSupportRequest = subOrderResponses.map((item) => ({
          ...item,
          key: item.subOrderId,
        }));
        setDriverIssues(mappedSupportRequest);
        // setDriverIssues(mockSubOrderResponses);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };
    if (user?.id) callGetSuborderIssue();
  }, [setLoading, user?.id]);

  const handleOk = async () => {
    const payload = {
      ...form.getFieldsValue(),
      reqTime: Date.now(),
      accessToken: getAccessToken(),
    };

    try {
      setLoading(true);
      const { orderId, reply } = form.getFieldsValue();
      await replySupportRequest(payload);
      alertSuccess('Phản hồi thành công');
      const cloneRequests = [...requests];
      const updatedRequests = cloneRequests.map((request) => {
        if (request.orderId === orderId) {
          request.reply = reply;
        }
        return request;
      });
      setRequests(updatedRequests);
    } catch (err) {
      alertError(err.message);
    } finally {
      form.resetFields();
      setLoading(false);
      setIsModalVisible(false);
    }
  };

  const handleOkIssue = async () => {
    const payload = {
      subOrderId: selectedIssue.subOrderId,
      newStatus,
      reqTime: Date.now(),
      accessToken: getAccessToken(),
    };
    try {
      setLoading(true);
      await updateSubOrderStatus(payload);
      alertSuccess('Cập nhật thành công');
      const newDriverIssues = driverIssues.filter((item) => item !== selectedIssue);
      setSelectedIssue(null);
      setDriverIssues(newDriverIssues);
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
      setIsStatusModalVisible(false);
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const onClick = (request: RequestsTable) => {
    setIsModalVisible(true);
    form.setFieldsValue({
      ...form.getFieldsValue(),
      orderId: request.orderId,
    });
  };

  const columns = [
    {
      title: 'Mã đơn hàng',
      render: (_: any, request: RequestsTable) => {
        return (
          <BlueText>
            <p>
              <FieldNumberOutlined /> {request.orderId}
            </p>
            <p>
              <CalendarOutlined /> {new Date(request.reqTime).toLocaleString()}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Nội dung',
      render: (_: any, request: RequestsTable) => {
        return (
          <BlueText>
            <div>{request.content}</div>
          </BlueText>
        );
      },
    },
    {
      title: 'Phản hồi',
      render: (_: any, request: RequestsTable) => {
        return (
          <BlueText>
            {request.reply || <a onClick={() => onClick(request)}>Click để phản hồi</a>}
          </BlueText>
        );
      },
    },
  ];

  const onDriverIssueClick = (issue: SubOrderResponse) => {
    setSelectedIssue(issue);
    setIsStatusModalVisible(true);
  };

  const driverIssueColumns = [
    {
      title: 'ID',
      render: (_: any, order: SubOrderResponse) => {
        const { subOrderId, reqTime, orderId, orderStatus } = order;
        return (
          <BlueText>
            <a onClick={() => onDriverIssueClick(order)}>Mã đơn: {subOrderId}</a>
            <p>Mã yêu cầu: {orderId}</p>
            <p>Trạng thái: {SuborderStatusText[orderStatus]}</p>
            <p>
              <CalendarOutlined /> {new Date(reqTime).toLocaleString()}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Người gửi',
      render: (_: any, order: SubOrderResponse) => {
        const { fromAddress, nameSender, phoneSender, emailSender }: DetailOrder = JSON.parse(
          order.detailOrder
        );
        return (
          <BlueText>
            <p>
              <UserOutlined /> {nameSender}
            </p>
            <p>
              <PhoneOutlined /> {phoneSender}
            </p>
            <p>
              <MailOutlined /> {emailSender}
            </p>
            <p>
              <HomeOutlined /> {fromAddress}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Người nhận',
      render: (_: any, order: SubOrderResponse) => {
        const { toAddress, nameReceiver, phoneReceiver, emailReceiver }: DetailOrder = JSON.parse(
          order.detailOrder
        );
        return (
          <BlueText>
            <p>
              <UserOutlined /> {nameReceiver}
            </p>
            <p>
              <PhoneOutlined /> {phoneReceiver}
            </p>
            <p>
              <MailOutlined /> {emailReceiver}
            </p>
            <p>
              <HomeOutlined /> {toAddress}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Hàng hóa',
      render: (_: any, order: SubOrderResponse) => {
        const { kind }: DetailOrder = JSON.parse(order.detailOrder);
        const { weight } = order;
        return (
          <BlueText>
            <p>
              <CodeSandboxOutlined /> {kind}
            </p>
            <p>
              <WeightIcon /> {weight}kg
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Báo cáo',
      render: (_: any, order: SubOrderResponse) => {
        const { issue } = order;
        return (
          <BlueText>
            <p>{issue}</p>
          </BlueText>
        );
      },
    },
  ];

  const onNewStatusChange = (newIssue: SuborderStatus) => {
    setNewStatus(newIssue);
  };

  return (
    <Container>
      <Tabs defaultActiveKey={history.location.hash} centered onChange={(key) => history.push(key)}>
        <TabPane tab="Yêu cầu đang chờ xử lí" key="#1">
          <Table
            dataSource={inProgressRequests}
            bordered
            columns={columns}
            locale={{ emptyText: <EmptyTableContent /> }}
          />
        </TabPane>
        <TabPane tab="Yêu cầu đã xử lí" key="#2">
          <Table
            dataSource={doneRequests}
            bordered
            columns={columns}
            locale={{ emptyText: <EmptyTableContent /> }}
          />
        </TabPane>
        <TabPane tab="Đơn hàng lỗi" key="#3">
          <Table
            dataSource={driverIssues}
            bordered
            columns={driverIssueColumns}
            locale={{ emptyText: <EmptyTableContent /> }}
          />
        </TabPane>
      </Tabs>

      <Modal
        title="Phản hồi yêu cầu"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Xác nhận"
      >
        <Form form={form} name="normal_login">
          <Label>Mã đơn hàng</Label>
          <Form.Item name="orderId">
            <PillInput disabled />
          </Form.Item>

          <Label>Nội dung phản hồi</Label>
          <Form.Item name="reply">
            <TextArea rows={4} placeholder="Nhập nội dung" />
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        title="Cập nhật trạng thái đơn hàng"
        visible={isStatusModalVisible}
        onOk={handleOkIssue}
        onCancel={() => setIsStatusModalVisible(false)}
        okText="Xác nhận"
      >
        <Form.Item label="Trạng thái mới">
          <PillSelect onChange={onNewStatusChange}>
            <Option value={SuborderStatus.FAILED}>{SuborderStatusText[-1]}</Option>
            <Option value={SuborderStatus.REQUESTED}>{SuborderStatusText[1]}</Option>
            <Option value={SuborderStatus.COMING_SEND_STOCK}>{SuborderStatusText[2]}</Option>
            <Option value={SuborderStatus.ARRIVED_SEND_STOCK}>{SuborderStatusText[3]}</Option>
            <Option value={SuborderStatus.TRANSFERING_ARRIVED_STOCK}>
              {SuborderStatusText[4]}
            </Option>
            <Option value={SuborderStatus.ARRIVED_DEST_STOCK}>{SuborderStatusText[5]}</Option>
            <Option value={SuborderStatus.TRANSFERING_TO_RECEIVER}>{SuborderStatusText[6]}</Option>
            <Option value={SuborderStatus.DONE}>{SuborderStatusText[7]}</Option>
          </PillSelect>
        </Form.Item>
      </Modal>
    </Container>
  );
};

const Container = styled.div`
  margin: auto;
  max-width: 90%;
  margin-bottom: 100px;
  margin-top: 50px;
`;

const { TabPane } = Tabs;
const { Option } = PillSelect;
