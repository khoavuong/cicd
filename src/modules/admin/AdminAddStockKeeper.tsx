import { Form } from 'antd';
import { FormInstance } from 'antd/lib/form';
import React, { useState } from 'react';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import styled from 'styled-components';
import { addStockKeeper } from '../../api/user';
import {
  EditButton,
  Label,
  PillInput,
  PillSelect,
} from '../../components/CommonStyles';
import { useLoading } from '../../components/LoadingProvider';
import { states } from '../../constants/states';
import { useWarehouses } from '../../hooks/useWarehouses';
import { AdminAddStockKeeperPayload } from '../../interface/Auth';
import { alertError, alertSuccess } from '../../utils/alert';
import { getGoogleApiKey } from '../../utils/getGoogleKey';

export const AdminAddStockKeeper = () => {
  const { warehouses } = useWarehouses();

  const [form]: FormInstance<AdminAddStockKeeperPayload>[] = Form.useForm();
  const { setLoading } = useLoading();
  const [, forceUpdate] = useState([]);

  const onFinish = async () => {
    const { password, rePassword } = form.getFieldsValue();
    if (password !== rePassword) {
      alertError('Nhập lại mật khẩu không khớp!');
      return;
    }

    try {
      setLoading(true);
      await addStockKeeper(form.getFieldsValue());
      form.resetFields();
      alertSuccess('Thêm thủ kho thành công');
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const onPlaceSearch = (value: any) => {
    const stateName = value.value.terms[value.value.terms.length - 2].value;
    const findProvinceCode =
      states.find((state) => state.label === stateName)?.value || 0;

    form.setFieldsValue({
      ...form.getFieldsValue(),
      provinceCode: findProvinceCode,
      address: value.label,
    });

    forceUpdate([]);
  };

  return (
    <Container>
      <Title>THÊM THỦ KHO</Title>
      <Form name="normal_login" onFinish={onFinish} form={form}>
        <Label>Email</Label>
        <Form.Item
          name="email"
          rules={[{ required: true, message: 'Vui lòng nhập email' }]}
        >
          <PillInput size="large" type="email" placeholder="Nhập email" />
        </Form.Item>

        <Label>Số điện thoại</Label>
        <Form.Item
          name="phone"
          rules={[{ required: true, message: 'Vui lòng nhập số điện thoại' }]}
        >
          <PillInput size="large" type="tel" placeholder="Nhập số điện thoại" />
        </Form.Item>

        <Label>Tên tài khoản</Label>
        <Form.Item
          name="username"
          rules={[{ required: true, message: 'Vui lòng nhập tên tài khoản' }]}
        >
          <PillInput
            size="large"
            type="text"
            placeholder="Nhập tên tài khoản"
          />
        </Form.Item>

        <Label>Địa chỉ</Label>
        <Form.Item
          name="address"
          rules={[{ required: true, message: 'Vui lòng nhập Địa chỉ' }]}
        >
          <GooglePlacesAutocomplete
            apiOptions={{ language: 'vi', region: 'vi' }}
            apiKey={getGoogleApiKey()}
            selectProps={{
              value: {
                label: form.getFieldValue('address') || '',
                value: form.getFieldValue('address') || '',
              },
              onChange: onPlaceSearch,
              styles: {
                control: (provided: any) => ({
                  ...provided,
                  borderRadius: '20px',
                }),
              },
            }}
          />
        </Form.Item>

        <Form.Item style={{ display: 'none' }} name="provinceCode" />

        <Label>Kho</Label>
        <Form.Item
          name="areaWarehouseId"
          rules={[{ required: true, message: 'Vui lòng nhập ID kho' }]}
        >
          <PillSelect placeholder="Chọn kho">
            {warehouses.map((warehouse) => (
              <Option key={warehouse.id} value={warehouse.id}>
                {warehouse.address}
              </Option>
            ))}
          </PillSelect>
        </Form.Item>

        <Label>Mật khẩu</Label>
        <Form.Item
          name="password"
          rules={[
            { required: true, message: 'Vui lòng nhập mật khẩu của bạn' },
          ]}
        >
          <PillInput size="large" type="password" placeholder="Nhập mật khẩu" />
        </Form.Item>

        <Label>Nhập lại mật khẩu</Label>
        <Form.Item
          name="rePassword"
          rules={[
            {
              required: true,
              message: 'Vui lòng xác nhận lại mật khẩu của bạn',
            },
          ]}
        >
          <PillInput
            size="large"
            type="password"
            placeholder="Nhập lại mật khẩu"
          />
        </Form.Item>

        <Form.Item>
          <EditButton size="large" type="primary" htmlType="submit">
            Đăng ký
          </EditButton>
        </Form.Item>
      </Form>
    </Container>
  );
};

const Container = styled.div`
  width: 500px;
  margin: auto;
  margin-top: 1rem;
`;

const Title = styled.h1`
  font-weight: bold;
  text-align: center;
`;

const { Option } = PillSelect;
