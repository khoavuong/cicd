/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react';
import { Table, Modal, Form } from 'antd';
import { BlueText, PillInput, PillInputNumber } from '../../components/CommonStyles';
import { useLoading } from '../../components/LoadingProvider';
import { alertError, alertSuccess } from '../../utils/alert';
import { getAccessToken } from '../../utils/accessToken';
import { addSubOrder, getOrderByUser } from '../../api/order';
import { DetailOrder, OrderByUser, OrderStatus } from '../../interface/Order';
import { useUser } from '../../components/UserProvider';
import { Container } from './StockMixin';
import { EmptyTableContent } from '../../components/EmptyTable';
import styled from 'styled-components';
import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  CalendarOutlined,
  FieldNumberOutlined,
  CodeSandboxOutlined,
  DollarOutlined,
} from '@ant-design/icons';
import { ReactComponent as WeightIcon } from '../../assets/weight.svg';
import { StockKeeperInterface } from '../../interface/Auth';
import ReactGA from 'react-ga';

export const StockImportSender = () => {
  const { user } = useUser();
  const { setLoading } = useLoading();
  const [requests, setRequests] = useState<OrderByUser[]>([]);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [selectedOrder, setSelectedOrder] = useState<OrderByUser>(null);
  const [weightToTake, setWeightToTake] = useState(null);
  const [form] = Form.useForm();

  const onRequestClick = (request: OrderByUser) => {
    setSelectedOrder(request);
    setWeightToTake(request.remainWeight);
    setIsModalVisible(true);
  };

  const requestColumns = [
    {
      title: 'Mã yêu cầu',
      render: (_: any, request: OrderByUser) => {
        const { orderId, orderTime } = request;
        return (
          <BlueText>
            <a onClick={() => onRequestClick(request)}>
              <FieldNumberOutlined /> {orderId}
            </a>
            <p>
              <CalendarOutlined /> {new Date(orderTime).toLocaleString()}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Bên gửi',
      render: (_: any, request: OrderByUser) => {
        const { nameSender, fromAddress, phoneSender, emailSender }: DetailOrder = JSON.parse(
          request.detailOrder
        );
        return (
          <BlueText>
            <p>
              <UserOutlined /> {nameSender}
            </p>
            <p>
              <PhoneOutlined /> {phoneSender}
            </p>
            <p>
              <MailOutlined /> {emailSender}
            </p>
            <p>
              <HomeOutlined /> {fromAddress}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Bên nhận',
      render: (_: any, request: OrderByUser) => {
        const { nameReceiver, toAddress, phoneReceiver, emailReceiver }: DetailOrder = JSON.parse(
          request.detailOrder
        );
        return (
          <BlueText>
            <p>
              <UserOutlined /> {nameReceiver}
            </p>
            <p>
              <PhoneOutlined /> {phoneReceiver}
            </p>
            <p>
              <MailOutlined /> {emailReceiver}
            </p>
            <p>
              <HomeOutlined /> {toAddress}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Hàng hóa',
      render: (_: any, request: OrderByUser) => {
        const { fee, totalWeight, remainWeight, paymentSide } = request;
        const { kind }: DetailOrder = JSON.parse(request.detailOrder);
        return (
          <BlueText>
            <p>
              <CodeSandboxOutlined /> {kind}
            </p>
            <p>
              <WeightIcon /> {remainWeight.toLocaleString()}/{totalWeight.toLocaleString()} kg
            </p>

            <p style={{ color: 'red' }}>
              <DollarOutlined />{' '}
              {fee.toLocaleString('it-IT', {
                style: 'currency',
                currency: 'VND',
              })}
            </p>
            {paymentSide === 'sender' ? <p>Bên gửi trả phí</p> : <p>Bên nhận trả phí</p>}
          </BlueText>
        );
      },
    },
    // {
    //   title: 'Thông tin thêm',
    //   render: (_: any, request: OrderByUser) => {
    //     const { description, value } = request;
    //     return (
    //       <BlueText>
    //         <p>Giá trị hàng hóa: {value.toLocaleString()}</p>
    //         <p>Ghi chú: {description}</p>
    //       </BlueText>
    //     );
    //   },
    // },
  ];

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const onInputChange = (value: number) => {
    setWeightToTake(value);
  };

  const handleOk = async () => {
    const payload = {
      weight: weightToTake,
      orderId: selectedOrder.orderId,
      accessToken: getAccessToken(),
      reqTime: Date.now(),
      stockKeeperId: user.id,
      stockId: (user as StockKeeperInterface).warehouseResponse.id,
    };

    try {
      setLoading(true);
      await addSubOrder(payload);
      ReactGA.event({
        category: 'Stockkeeper',
        action: 'Stockkeeper import sender',
      });

      alertSuccess('Nhập kho thành công');
      form.resetFields();
      setRequests([]);
      setIsModalVisible(false);
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const renderModal = () => {
    if (!selectedOrder) return null;

    return (
      <Modal
        title="Nhập khối lượng muốn lấy"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Chấp nhận"
      >
        <Form.Item label="Nhập khối lượng muốn lấy">
          <PillInputNumber
            style={{ width: '100%' }}
            value={weightToTake}
            onChange={onInputChange}
          />
        </Form.Item>
      </Modal>
    );
  };

  const onFinish = async (request: { senderId: string }) => {
    const payload = {
      userId: request.senderId,
      accessToken: getAccessToken(),
      reqTime: Date.now(),
    };

    try {
      setLoading(true);
      const { orders } = await getOrderByUser(payload);
      const ordersTableData = orders?.map((order) => {
        return {
          key: order.orderId,
          ...order,
        };
      });
      const filteredOrdersTableData = ordersTableData.filter(
        (order) =>
          order.directlySend &&
          order.orderStatus !== OrderStatus.CANCELED &&
          order.srcStockId === (user as StockKeeperInterface).warehouseResponse.id
      );
      setRequests(filteredOrdersTableData);
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Container>
      <CenterInput>
        <Form form={form} onFinish={onFinish}>
          <Form.Item style={{ marginBottom: 0 }} label="Nhập ID khách hàng:" name="senderId">
            <PillInput />
          </Form.Item>
        </Form>
      </CenterInput>
      <br />
      <br />
      <Table
        bordered
        columns={requestColumns}
        dataSource={requests}
        pagination={{ pageSize: 3, hideOnSinglePage: true }}
        locale={{ emptyText: <EmptyTableContent /> }}
      />
      {renderModal()}
    </Container>
  );
};

const CenterInput = styled.div`
  display: flex;
  justify-content: center;
`;
