/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Tabs } from 'antd';
import styled from 'styled-components';
import { useHistory } from 'react-router';
import { StockExportDriver } from './StockExportDriver';
import { StockExportReceiver } from './StockExportReceiver';

export const StockExport = () => {
  const history = useHistory();

  const onTabChange = (key: any) => {
    history.push(key);
  };

  return (
    <Container>
      <Tabs
        defaultActiveKey={history.location.hash}
        centered
        onChange={onTabChange}
      >
        <TabPane tab="Xuất đơn tài xế" key="#1">
          <StockExportDriver />
        </TabPane>
        <TabPane tab="Xuất đơn cho người nhận" key="#2">
          <StockExportReceiver />
        </TabPane>
      </Tabs>
    </Container>
  );
};

const { TabPane } = Tabs;

const Container = styled.div`
  margin: auto;
  max-width: 90%;
  margin-bottom: 100px;
  margin-top: 13px;
`;
