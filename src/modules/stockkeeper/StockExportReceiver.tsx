/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react';
import { Table, Modal, Form } from 'antd';
import { BlueText, PillButton, PillInput } from '../../components/CommonStyles';
import { useLoading } from '../../components/LoadingProvider';
import { alertError, alertSuccess } from '../../utils/alert';
import { getAccessToken } from '../../utils/accessToken';
import { getSuborderByOrder, receiveSubOrder } from '../../api/order';
import { DetailOrder, SubOrderResponse, SuborderStatus } from '../../interface/Order';
import { useUser } from '../../components/UserProvider';
import { Container } from './StockMixin';
import { EmptyTableContent } from '../../components/EmptyTable';
import styled from 'styled-components';
import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  FieldNumberOutlined,
  CodeSandboxOutlined,
} from '@ant-design/icons';
import { ReactComponent as WeightIcon } from '../../assets/weight.svg';
import { StockKeeperInterface } from '../../interface/Auth';
import ReactGA from 'react-ga';

export const StockExportReceiver = () => {
  const { user } = useUser();
  const { setLoading } = useLoading();
  const [subOrders, setSubOrders] = useState<SubOrderResponse[]>([]);

  const [form] = Form.useForm();

  const requestColumns = [
    {
      title: 'Mã đơn',
      render: (_: any, request: SubOrderResponse) => {
        const { subOrderId } = request;
        return (
          <BlueText>
            <p>
              <FieldNumberOutlined /> {subOrderId}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Bên gửi',
      render: (_: any, request: SubOrderResponse) => {
        const { nameSender, fromAddress, phoneSender, emailSender }: DetailOrder = JSON.parse(
          request.detailOrder
        );
        return (
          <BlueText>
            <p>
              <UserOutlined /> {nameSender}
            </p>
            <p>
              <PhoneOutlined /> {phoneSender}
            </p>
            <p>
              <MailOutlined /> {emailSender}
            </p>
            <p>
              <HomeOutlined /> {fromAddress}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Bên nhận',
      render: (_: any, request: SubOrderResponse) => {
        const { nameReceiver, toAddress, phoneReceiver, emailReceiver }: DetailOrder = JSON.parse(
          request.detailOrder
        );
        return (
          <BlueText>
            <p>
              <UserOutlined /> {nameReceiver}
            </p>
            <p>
              <PhoneOutlined /> {phoneReceiver}
            </p>
            <p>
              <MailOutlined /> {emailReceiver}
            </p>
            <p>
              <HomeOutlined /> {toAddress}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Hàng hóa',
      render: (_: any, request: SubOrderResponse) => {
        const { weight } = request;
        const { kind }: DetailOrder = JSON.parse(request.detailOrder);
        return (
          <BlueText>
            <p>
              <CodeSandboxOutlined /> {kind}
            </p>
            <p>
              <WeightIcon /> {weight.toLocaleString()} kg
            </p>
          </BlueText>
        );
      },
    },
    // {
    //   title: 'Thông tin thêm',
    //   render: (_: any, request: SubOrderResponse) => {
    //     const { description, value } = request;
    //     return (
    //       <BlueText>
    //         <p>Giá trị hàng hóa: {value.toLocaleString()}</p>
    //         <p>Ghi chú: {description}</p>
    //       </BlueText>
    //     );
    //   },
    // },
  ];

  const onFinish = async (request: { orderId: string }) => {
    const payload = {
      orderId: request.orderId,
      accessToken: getAccessToken(),
      reqTime: Date.now(),
    };

    try {
      setLoading(true);
      const { subOrderResponses } = await getSuborderByOrder(payload);
      const ordersTableData = subOrderResponses?.map((order) => {
        return {
          key: order.subOrderId,
          ...order,
        };
      });
      const filteredSubOrder = ordersTableData.filter(
        (item) => item.orderStatus === SuborderStatus.ARRIVED_DEST_STOCK
      );

      setSubOrders(filteredSubOrder);
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const onExportOrders = async () => {
    const subOrderIds = subOrders.map((order) => order.subOrderId);
    const payload = {
      accessToken: getAccessToken(),
      reqTime: Date.now(),
      subOrderIds,
      stockKeeperId: user.id,
      stockId: (user as StockKeeperInterface).warehouseResponse.id,
    };
    try {
      await receiveSubOrder(payload);
      ReactGA.event({
        category: 'Stockkeeper',
        action: 'Stockkeeper export receiver',
      });
      alertSuccess('Xuất hàng cho người nhận thành công');
      form.resetFields();
      setSubOrders([]);
    } catch (err) {
      alertError(err.message);
    }
  };

  const onClick = () => {
    Modal.confirm({
      title: 'Xuất các đơn hàng của yêu cầu này ra kho?',
      onOk: onExportOrders,
    });
  };

  return (
    <Container>
      <CenterInput>
        <Form form={form} onFinish={onFinish}>
          <Form.Item style={{ marginBottom: 0 }} label="Nhập ID yêu cầu:" name="orderId">
            <PillInput />
          </Form.Item>
        </Form>
      </CenterInput>
      <br />
      <PillButton onClick={onClick} type="primary">
        Xuất đơn hàng cho người nhận
      </PillButton>
      <br />
      <br />
      <Table
        bordered
        columns={requestColumns}
        dataSource={subOrders}
        pagination={{ pageSize: 3, hideOnSinglePage: true }}
        locale={{ emptyText: <EmptyTableContent /> }}
      />
    </Container>
  );
};

const CenterInput = styled.div`
  display: flex;
  justify-content: center;
`;
