import React, { useState } from 'react';
import { Table, Space, Modal } from 'antd';
import { FlexCenter, PillButton, PillSelect } from '../../components/CommonStyles';
import { useLoading } from '../../components/LoadingProvider';
import { alertError } from '../../utils/alert';
import { getAccessToken } from '../../utils/accessToken';
import { exportSubOrders } from '../../api/order';
import { SuborderStatus } from '../../interface/Order';
import { useUser } from '../../components/UserProvider';
import { GetListDriverResponse, StockKeeperInterface } from '../../interface/Auth';
import { PDFDownloadLink } from '@react-pdf/renderer';
import { ImportExportOrdersPDF } from './ImportExportOrdersPDF';
import {
  Container,
  getOrdersTableDataByDriver,
  ImportExport,
  requestColumns,
  FlattenSubOrderByDriver,
} from './StockMixin';
import { useGetListDriver } from '../../hooks/useGetListDriver';
import { EmptyTableContent } from '../../components/EmptyTable';
import ReactGA from 'react-ga';

export const StockExportDriver = () => {
  const { user } = useUser();
  const { drivers } = useGetListDriver();
  const { setLoading } = useLoading();
  const [selectedDriver, setSelectedDriver] = useState<GetListDriverResponse>(null);
  const [orders, setOrders] = useState<FlattenSubOrderByDriver[]>(null);
  let exportTime: number;

  const onExportOrders = async () => {
    const subOrderIds = orders.map((order) => order.subOrderId);
    exportTime = Date.now();
    const payload = {
      accessToken: getAccessToken(),
      reqTime: exportTime,

      subOrderIds,
      stockKeeperId: user.id,
      stockId: (user as StockKeeperInterface).warehouseResponse.id,
      driverId: selectedDriver.id,

      stockKeeperName: (user as StockKeeperInterface).username,
      driverName: selectedDriver.username,
      stockKeeperPhone: (user as StockKeeperInterface).phone,
      driverPhone: selectedDriver.phone || '09180321242',
    };
    try {
      await exportSubOrders(payload);
      ReactGA.event({
        category: 'Stockkeeper',
        action: 'Stockkeeper export driver',
      });
      Modal.success({
        title: 'Xuất đơn hàng thành công',
        content: (
          <PDFDownloadLink
            document={
              <ImportExportOrdersPDF
                type={ImportExport.Export}
                driver={selectedDriver}
                stockKeeper={user as StockKeeperInterface}
                orders={orders}
                time={exportTime}
              />
            }
            fileName="export.pdf"
          >
            Tải phiếu xuất kho
          </PDFDownloadLink>
        ),
      });

      setOrders(null);
      setSelectedDriver(null);
    } catch (err) {
      alertError(err.message);
    }
  };

  const onClick = () => {
    Modal.confirm({
      title: 'Xuất đơn hàng trong xe ra kho?',
      onOk: onExportOrders,
    });
  };

  const findOrder = async (value: string) => {
    setSelectedDriver(drivers.find((driver) => driver.id === value));

    try {
      setLoading(true);
      const ordersTableData = await getOrdersTableDataByDriver(value);
      const filtedOrdersTableData = ordersTableData.filter(
        (subOrder) =>
          subOrder.orderStatus === SuborderStatus.ARRIVED_SEND_STOCK ||
          subOrder.orderStatus === SuborderStatus.ARRIVED_DEST_STOCK
      );

      setOrders(filtedOrdersTableData);
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Container>
      <FlexCenter>
        <Space>
          <span>
            <strong>Nhập ID tài xế: </strong>
          </span>
          <PillSelect
            showSearch
            style={{ width: 200 }}
            value={selectedDriver?.id}
            onChange={findOrder}
            filterOption={true}
          >
            {drivers.map((driver) => (
              <Option key={driver.id} value={driver.id}>
                {driver.id}
              </Option>
            ))}
          </PillSelect>
        </Space>
      </FlexCenter>
      <br />
      <PillButton onClick={onClick} type="primary">
        Xuất đơn hàng trong xe ra kho
      </PillButton>
      <br />
      <br />
      <Table
        bordered
        columns={requestColumns}
        dataSource={orders}
        pagination={{ pageSize: 3, hideOnSinglePage: true }}
        locale={{ emptyText: <EmptyTableContent /> }}
      />
    </Container>
  );
};

const { Option } = PillSelect;
