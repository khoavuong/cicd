import React, { ReactNode, useState } from 'react';
import { useHistory } from 'react-router';
import { Layout, Menu } from 'antd';
import {
  UserOutlined,
  UnorderedListOutlined,
  ImportOutlined,
  ExportOutlined,
  HistoryOutlined,
  LogoutOutlined,
} from '@ant-design/icons';
import {
  LayoutContent,
  Logo,
  User,
  UserIconWrapper,
  UserName,
} from '../../components/CommonStyles';
import { useGetUser } from '../../components/UserProvider';
import { ReactComponent as LogoImg } from '../../assets/logo.svg';
import userIcon from '../../assets/user.svg';

export const StockNavigation = ({ children }: { children: ReactNode }) => {
  const { user } = useGetUser();
  const [, forceUpdate] = useState('');
  const history = useHistory();

  const handleClick = (e: any) => {
    forceUpdate(e.key);
    if (e.key === 'logout') {
      history.push(`/signin`);
    } else {
      history.push(`/stockkeeper/${e.key}`);
    }
  };

  return (
    <Layout style={{ height: '100%' }}>
      <Sider
        style={{ backgroundColor: '#00467f' }}
        breakpoint="lg"
        collapsedWidth="0"
      >
        <Logo onClick={() => history.push('/')}>
          <LogoImg />
        </Logo>
        <User>
          <UserIconWrapper>
            <img src={userIcon} alt="user" />
          </UserIconWrapper>
          <UserName>{user?.username}</UserName>
        </User>{' '}
        <Menu
          style={{ backgroundColor: '#00467f' }}
          theme="dark"
          mode="inline"
          onClick={handleClick}
          selectedKeys={[history.location.pathname.split('/')[2]]}
        >
          <Menu.Item key="account" icon={<UserOutlined />}>
            Chỉnh sửa thông tin
          </Menu.Item>
          <Menu.Item key="import" icon={<ImportOutlined />}>
            Nhập hàng vào kho
          </Menu.Item>
          <Menu.Item key="export" icon={<ExportOutlined />}>
            Xuất hàng ra kho
          </Menu.Item>
          <Menu.Item key="instock" icon={<UnorderedListOutlined />}>
            Đơn hàng trong kho
          </Menu.Item>
          <Menu.Item key="history" icon={<HistoryOutlined />}>
            Lịch sử nhập/xuất
          </Menu.Item>
          <Menu.Item key="logout" icon={<LogoutOutlined />}>
            Đăng xuất
          </Menu.Item>
        </Menu>
      </Sider>

      <Layout>
        <LayoutContent>{children}</LayoutContent>
      </Layout>
    </Layout>
  );
};

const { Sider } = Layout;
