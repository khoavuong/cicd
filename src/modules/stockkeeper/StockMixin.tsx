import React from 'react';
import { DetailOrder, SubOrderByDriver } from '../../interface/Order';
import styled from 'styled-components';
import { getSubOrderByDriver } from '../../api/order';
import { getAccessToken } from '../../utils/accessToken';
import { BlueText } from '../../components/CommonStyles';
import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  CodeSandboxOutlined,
} from '@ant-design/icons';

export enum ImportExport {
  Import = 'nhập',
  Export = 'xuất',
}

export interface FlattenSubOrderByDriver extends SubOrderByDriver, DetailOrder {
  key: string;
}

export const requestColumns = [
  {
    title: 'Mã yêu cầu / đơn',
    render: (_: any, order: FlattenSubOrderByDriver) => {
      const { subOrderId, orderId } = order;
      return (
        <BlueText>
          <p>Mã đơn: {subOrderId}</p>
          <p>Mã yêu cầu: {orderId}</p>
        </BlueText>
      );
    },
  },
  {
    title: 'Thông tin người gửi',
    render: (_: any, order: FlattenSubOrderByDriver) => {
      const { nameSender, fromAddress, phoneSender, emailSender } = order;

      return (
        <BlueText>
          <p>
            <UserOutlined /> {nameSender}
          </p>
          <p>
            <PhoneOutlined /> {phoneSender}
          </p>
          <p>
            <MailOutlined /> {emailSender}
          </p>
          <p>
            <HomeOutlined /> {fromAddress}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Thông tin người nhận',
    render: (_: any, order: FlattenSubOrderByDriver) => {
      const { nameReceiver, toAddress, phoneReceiver, emailReceiver } = order;

      return (
        <BlueText>
          <p>
            <UserOutlined /> {nameReceiver}
          </p>
          <p>
            <PhoneOutlined /> {phoneReceiver}
          </p>
          <p>
            <MailOutlined /> {emailReceiver}
          </p>
          <p>
            <HomeOutlined /> {toAddress}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Hàng hóa',
    render: (_: any, order: FlattenSubOrderByDriver) => {
      const { weight, kind, totalWeight } = order;
      return (
        <>
          <p>
            <CodeSandboxOutlined /> {kind}
          </p>
          <p style={{ color: 'red' }}>
            Khối lượng: {weight}kg/{totalWeight}kg
          </p>
        </>
      );
    },
  },
];

export const Container = styled.div`
  margin: auto;
  max-width: 90%;
`;

export const getOrdersTableDataByDriver = async (driverId: string) => {
  const payload = {
    driverId,
    accessToken: getAccessToken(),
    reqTime: Date.now(),
  };

  const { orders } = await getSubOrderByDriver(payload);
  const ordersTableData = orders.map((order) => {
    const detailOrder: DetailOrder = JSON.parse(order.detailOrder);
    return {
      key: order.subOrderId,
      ...order,
      ...detailOrder,
    };
  });

  return ordersTableData;
};
