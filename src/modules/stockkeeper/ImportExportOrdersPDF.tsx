import React, { Fragment } from 'react';
import { Page, Text, Document, StyleSheet, Font } from '@react-pdf/renderer';
import {
  GetListDriverResponse,
  StockKeeperInterface,
} from '../../interface/Auth';
import { ImportExport, FlattenSubOrderByDriver } from './StockMixin';

interface ImportExportOrdersPDFProps {
  driver?: GetListDriverResponse;
  driverId?: string;
  stockKeeper: StockKeeperInterface;
  orders: FlattenSubOrderByDriver[];
  type: ImportExport;
  time: number;
}

export const ImportExportOrdersPDF = ({
  driver,
  stockKeeper,
  orders,
  type,
  driverId,
  time,
}: ImportExportOrdersPDFProps) => {
  return (
    <Document>
      <Page style={styles.body}>
        <Text style={styles.header} fixed>
          BKShipment
        </Text>
        <Text style={styles.title}>Đơn {type} kho</Text>
        <Text style={styles.author}>
          {stockKeeper.warehouseResponse.address}
        </Text>
        <Text style={styles.text}>
          Thủ kho: {stockKeeper.username}, ID: {stockKeeper.id}, SDT:{' '}
          {stockKeeper.phone}
        </Text>

        {driver && (
          <Text style={styles.text}>
            Tài xế: ID: {driver.id}, Tên: {driver.username}, vehicleId:{' '}
            {driver.vehicleResponse.id}
          </Text>
        )}

        {driverId !== '0' && driverId !== undefined && !driver && (
          <Text style={styles.text}>Tài xế: ID: {driverId}</Text>
        )}

        <Text style={styles.text}>
          Thời gian: {new Date(time).toLocaleString()}
        </Text>

        <Text style={styles.hr} />

        {renderOrders(orders)}

        <Text
          style={styles.pageNumber}
          render={({ pageNumber, totalPages }) =>
            `${pageNumber} / ${totalPages}`
          }
          fixed
        />
      </Page>
    </Document>
  );
};

const renderOrders = (orders: FlattenSubOrderByDriver[]) => {
  return orders.map((order) => (
    <Fragment key={order.subOrderId}>
      <Text style={styles.text}>Đơn hàng: {order.subOrderId}</Text>

      <Text style={styles.text}>
        Thông tin người gửi: {order.nameSender}, {order.phoneSender},{' '}
        {order.fromAddress}
      </Text>

      <Text style={styles.text}>
        Thông tin người nhận: {order.nameReceiver}, {order.phoneReceiver},{' '}
        {order.toAddress}
      </Text>

      <Text style={styles.text}>
        Hàng hóa: {order.kind}, Khối lượng: {order.weight}
      </Text>

      <Text style={styles.hr} />
    </Fragment>
  ));
};

Font.register({
  family: 'Roboto',
  src: 'https://fonts.gstatic.com/s/roboto/v27/KFOmCnqEu92Fr1Me5Q.ttf',
});

const styles = StyleSheet.create({
  body: {
    paddingTop: 35,
    paddingBottom: 65,
    paddingHorizontal: 35,
    fontFamily: 'Roboto',
  },
  title: {
    fontSize: 24,
    textAlign: 'center',
    fontFamily: 'Roboto',
  },
  author: {
    fontSize: 12,
    textAlign: 'center',
    marginBottom: 40,
    fontFamily: 'Roboto',
  },
  text: {
    margin: 12,
    fontSize: 14,
    textAlign: 'justify',
  },
  header: {
    fontSize: 12,
    marginBottom: 20,
    textAlign: 'center',
    color: 'grey',
    fontFamily: 'Roboto',
  },
  pageNumber: {
    position: 'absolute',
    fontSize: 12,
    bottom: 30,
    left: 0,
    right: 0,
    textAlign: 'center',
    color: 'grey',
    fontFamily: 'Roboto',
  },
  hr: {
    width: '100%',
    border: 1,
  },
});
