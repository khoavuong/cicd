import { Table } from 'antd';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { getSubOrderByStock } from '../../api/order';
import { BlueText } from '../../components/CommonStyles';
import { EmptyTableContent } from '../../components/EmptyTable';
import { useLoading } from '../../components/LoadingProvider';
import { useUser } from '../../components/UserProvider';
import { StockKeeperInterface } from '../../interface/Auth';
import { DetailOrder, SubOrderResponse } from '../../interface/Order';
import { getAccessToken } from '../../utils/accessToken';
import { alertError } from '../../utils/alert';
import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  CodeSandboxOutlined,
  FieldNumberOutlined,
} from '@ant-design/icons';
// import { mockSubOrderResponses } from '../../constants/mockTableData';

export const StockList = () => {
  const { user } = useUser();
  const { setLoading } = useLoading();
  const [orders, setOrders] = useState<SubOrderResponse[]>(null);
  useEffect(() => {
    const callGetSubOrderByStock = async () => {
      const payload = {
        stockId: (user as StockKeeperInterface)?.warehouseResponse.id,
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };

      try {
        setLoading(true);
        const { orders } = await getSubOrderByStock(payload);
        const tableDataSrc = orders.map((order) => {
          return {
            key: order.subOrderId,
            ...order,
          };
        });
        setOrders(tableDataSrc);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };
    if (user) callGetSubOrderByStock();
  }, [setLoading, user]);

  return (
    <OrdersContainer>
      <Title>
        <BlueText>Danh sách đơn hàng trong kho</BlueText>
        <BlueText>{(user as StockKeeperInterface)?.warehouseResponse.address}</BlueText>
      </Title>
      <br />
      <Table
        bordered
        columns={columns}
        dataSource={orders}
        // dataSource={mockSubOrderResponses}
        locale={{ emptyText: <EmptyTableContent /> }}
      />
    </OrdersContainer>
  );
};

const OrdersContainer = styled.div`
  margin: auto;
  max-width: 90%;
  margin-bottom: 100px;
  margin-top: 50px;
`;

const Title = styled.div`
  text-align: center;
  font-size: 20px;
`;

const columns = [
  {
    title: 'Mã yêu cầu / đơn',
    render: (_: any, order: SubOrderResponse) => {
      const { subOrderId, orderId } = order;
      return (
        <BlueText>
          <p>Mã đơn: {subOrderId}</p>
          <p>Mã yêu cầu: {orderId}</p>
        </BlueText>
      );
    },
  },
  {
    title: 'Thông tin người gửi',
    render: (_: any, order: SubOrderResponse) => {
      const { nameSender, fromAddress, phoneSender, emailSender }: DetailOrder = JSON.parse(
        order.detailOrder
      );

      return (
        <BlueText>
          <p>
            <UserOutlined /> {nameSender}
          </p>
          <p>
            <PhoneOutlined /> {phoneSender}
          </p>
          <p>
            <MailOutlined /> {emailSender}
          </p>
          <p>
            <HomeOutlined /> {fromAddress}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Thông tin người nhận',
    render: (_: any, order: SubOrderResponse) => {
      const { nameReceiver, toAddress, phoneReceiver, emailReceiver }: DetailOrder = JSON.parse(
        order.detailOrder
      );

      return (
        <BlueText>
          <p>
            <UserOutlined /> {nameReceiver}
          </p>
          <p>
            <PhoneOutlined /> {phoneReceiver}
          </p>
          <p>
            <MailOutlined /> {emailReceiver}
          </p>
          <p>
            <HomeOutlined /> {toAddress}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Hàng hóa',
    render: (_: any, order: SubOrderResponse) => {
      const { weight, detailOrder, totalWeight } = order;
      const { kind }: DetailOrder = JSON.parse(detailOrder);

      return (
        <>
          <p>
            <CodeSandboxOutlined /> {kind}
          </p>
          <p style={{ color: 'red' }}>
            Khối lượng: {weight}kg/{totalWeight}kg
          </p>
        </>
      );
    },
  },
  {
    title: 'Tài xế phụ trách',
    render: (_: any, order: SubOrderResponse) => {
      const {
        driverId,
        externalDriverId,
        destDriverId,
        driverName,
        driverPhone,
        destDriverName,
        destDriverPhone,
        externalDriverName,
        externalDriverPhone,
      } = order;
      return (
        <BlueText>
          {driverId ? (
            <div>
              <p style={{ color: 'red' }}>Tài nội tỉnh đi:</p>
              <p>
                <FieldNumberOutlined /> {driverId}
              </p>
              <p>
                <UserOutlined /> {driverName}
              </p>
              <p>
                <PhoneOutlined /> {driverPhone}
              </p>
            </div>
          ) : null}
          {externalDriverId ? (
            <div>
              <br />
              <p style={{ color: 'red' }}>Tài liên tỉnh:</p>
              <p>
                <FieldNumberOutlined /> {externalDriverId}
              </p>
              <p>
                <UserOutlined /> {externalDriverName}
              </p>
              <p>
                <PhoneOutlined /> {externalDriverPhone}
              </p>
            </div>
          ) : null}
          {destDriverId ? (
            <div>
              <br />
              <p style={{ color: 'red' }}>Tài nội tỉnh đến:</p>
              <p>
                <FieldNumberOutlined /> {destDriverId}
              </p>
              <p>
                <UserOutlined /> {destDriverName}
              </p>
              <p>
                <PhoneOutlined /> {destDriverPhone}
              </p>
            </div>
          ) : null}
        </BlueText>
      );
    },
  },
];
