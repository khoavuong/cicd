/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Tabs } from 'antd';
import styled from 'styled-components';
import { useHistory } from 'react-router';
import { StockImportSender } from './StockImportSender';
import { StockImportDriver } from './StockImportDriver';

export const StockImport = () => {
  const history = useHistory();

  const onTabChange = (key: any) => {
    history.push(key);
  };

  return (
    <Container>
      <Tabs
        defaultActiveKey={history.location.hash}
        centered
        onChange={onTabChange}
      >
        <TabPane tab="Nhập đơn tài xế" key="#1">
          <StockImportDriver />
        </TabPane>
        <TabPane tab="Nhập đơn khách hàng" key="#2">
          <StockImportSender />
        </TabPane>
      </Tabs>
    </Container>
  );
};

const { TabPane } = Tabs;

const Container = styled.div`
  margin: auto;
  max-width: 90%;
  margin-bottom: 100px;
  margin-top: 13px;
`;
