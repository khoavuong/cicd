import React, { ReactNode, useState } from 'react';
import { useHistory } from 'react-router';
import { Layout, Menu } from 'antd';
import {
  UserOutlined,
  UnorderedListOutlined,
  HistoryOutlined,
  LogoutOutlined,
} from '@ant-design/icons';
import {
  LayoutContent,
  Logo,
  User,
  UserIconWrapper,
  UserName,
} from '../../components/CommonStyles';
import { useGetUser } from '../../components/UserProvider';
import { ReactComponent as LogoImg } from '../../assets/logo.svg';
import userIcon from '../../assets/user.svg';

export const DriverNavigation = ({ children }: { children: ReactNode }) => {
  const { user } = useGetUser();
  const [, forceUpdate] = useState('');
  const history = useHistory();

  const handleClick = (e: any) => {
    forceUpdate(e.key);
    if (e.key === 'logout') {
      history.push(`/signin`);
    } else {
      history.push(`/driver/${e.key}`);
    }
  };

  return (
    <Layout style={{ height: '100%' }}>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        style={{ backgroundColor: '#00467f' }}
      >
        <Logo onClick={() => history.push('/')}>
          <LogoImg />
        </Logo>

        <User>
          <UserIconWrapper>
            <img src={userIcon} alt="user" />
          </UserIconWrapper>
          <UserName>{user?.username}</UserName>
        </User>

        <Menu
          style={{ backgroundColor: '#00467f' }}
          theme="dark"
          mode="inline"
          onClick={handleClick}
          selectedKeys={[history.location.pathname.split('/')[2]]}
        >
          <Menu.Item key="account" icon={<UserOutlined />}>
            Chỉnh sửa thông tin
          </Menu.Item>
          <Menu.Item key="orders" icon={<UnorderedListOutlined />}>
            Đơn hàng thực hiện
          </Menu.Item>
          <Menu.Item key="history" icon={<HistoryOutlined />}>
            Lịch sử giao nhận
          </Menu.Item>
          <Menu.Item key="logout" icon={<LogoutOutlined />}>
            Đăng xuất
          </Menu.Item>
        </Menu>
      </Sider>

      <Layout>
        <LayoutContent>{children}</LayoutContent>
      </Layout>
    </Layout>
  );
};

const { Sider } = Layout;
