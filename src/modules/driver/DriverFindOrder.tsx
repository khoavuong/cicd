/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from 'react';
import { Modal, Table } from 'antd';
import { useLoading } from '../../components/LoadingProvider';
import { useUser } from '../../components/UserProvider';
import { getAccessToken } from '../../utils/accessToken';
import { acceptOrder, findOrder } from '../../api/order';
import { alertError, alertSuccess } from '../../utils/alert';
import {
  BlueText,
  FlexSpaceBetween,
  InfoContainer,
  InfoTitle,
} from '../../components/CommonStyles';
import {
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  FieldNumberOutlined,
  CodeSandboxOutlined,
} from '@ant-design/icons';
import { FoundOrder } from '../../interface/Order';
import { useHistory } from 'react-router';
import { EmptyTableContent } from '../../components/EmptyTable';
// import { driverFindOrder } from '../../constants/mockTableData';
import { ReactComponent as WeightIcon } from '../../assets/weight.svg';
import ReactGA from 'react-ga';

export const DriverFindOrder = () => {
  const history = useHistory();
  const { user } = useUser();
  const { setLoading } = useLoading();

  const [orders, setOrders] = useState<FoundOrder[]>(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedOrder, setSelectedOrder] = useState<FoundOrder>(null);

  useEffect(() => {
    const callFindOrder = async () => {
      const payload = {
        driverId: user.id,
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };
      try {
        setLoading(true);
        const { orders } = await findOrder(payload);
        if (!orders || !orders.length) return;
        const ordersTableData = orders.map((order) => {
          return {
            key: order.orderId,
            ...order,
          };
        });
        setOrders(ordersTableData);

        // setOrders(driverFindOrder);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    if (user && (history.location.hash === '' || history.location.hash === '#1')) {
      callFindOrder();
    }
  }, [setLoading, user, history.location.hash]);

  const requestColumns = [
    {
      title: 'Mã yêu cầu',
      render: (_: any, order: FoundOrder) => {
        const { orderId } = order;
        return (
          <BlueText>
            <FieldNumberOutlined /> <a onClick={() => onOrderClick(order)}>{orderId}</a>
          </BlueText>
        );
      },
    },
    {
      title: 'Thông tin khách hàng',
      render: (_: any, order: FoundOrder) => {
        const { nameSender, addressSender, phoneSender } = order;
        return (
          <BlueText>
            <p>
              <UserOutlined /> {nameSender}
            </p>
            <p>
              <PhoneOutlined /> {phoneSender}
            </p>
            <p>
              <HomeOutlined /> {addressSender}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Hàng hóa',
      render: (_: any, order: FoundOrder) => {
        const { remainWeight, totalWeight, kind } = order;
        return (
          <BlueText>
            <p>
              <CodeSandboxOutlined /> {kind}
            </p>
            <p>
              <WeightIcon /> Tổng khối lượng: {totalWeight}kg
            </p>
            <p style={{ color: 'red' }}>Còn lại: {remainWeight}kg</p>
          </BlueText>
        );
      },
    },
  ];

  const onOrderClick = (order: FoundOrder) => {
    setSelectedOrder(order);
    setIsModalVisible(true);
  };

  const handleOk = async () => {
    const payload = {
      driverId: user.id,
      orderId: selectedOrder.orderId,
      driverName: user.username,
      driverPhone: user.phone,
      accessToken: getAccessToken(),
      reqTime: Date.now(),
    };
    try {
      setLoading(true);
      await acceptOrder(payload);
      ReactGA.event({
        category: 'Driver',
        action: 'Driver accepts order',
      });
      setOrders(orders.filter((order) => order !== selectedOrder));
      alertSuccess('Nhận yêu cầu thành công');
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <div>
      <Table
        bordered
        columns={requestColumns}
        dataSource={orders}
        pagination={{ pageSize: 3, hideOnSinglePage: true }}
        locale={{ emptyText: <EmptyTableContent /> }}
      />

      <br />
      {selectedOrder && (
        <Modal
          width={700}
          title="Thông tin đơn hàng"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          okText="Chấp nhận"
        >
          <FlexSpaceBetween>
            <div>
              <InfoTitle>| Bên nhận</InfoTitle>
              <InfoContainer>
                <p>Số điện thoại: {selectedOrder.phoneSender}</p>
                <p>Họ tên: {selectedOrder.nameSender}</p>
                <p>Địa chỉ: {selectedOrder.addressSender}</p>
              </InfoContainer>
            </div>

            <div>
              <InfoTitle>| Hàng hoá</InfoTitle>
              <InfoContainer>
                <p>Loại hàng: {selectedOrder.kind}</p>
                <p>Khối lượng tổng: {selectedOrder.totalWeight}</p>
                <p>Khối lượng chưa lấy: {selectedOrder.remainWeight}</p>
              </InfoContainer>
            </div>
          </FlexSpaceBetween>
        </Modal>
      )}
    </div>
  );
};
