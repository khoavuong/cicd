import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'antd/dist/antd.css';
import './index.css';
import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';
import ReactGA from 'react-ga';

const TRACKING_ID = 'UA-157783698-1'; // YOUR_OWN_TRACKING_ID
ReactGA.initialize(TRACKING_ID);

Sentry.init({
  dsn: 'https://4f149e872ed44f6d80dba511a08d064a@o889085.ingest.sentry.io/5838540',
  integrations: [new Integrations.BrowserTracing()],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
});

ReactDOM.render(<App />, document.getElementById('root'));
