export const states = [
  { value: 0, label: 'None' },
  { value: 1, label: 'Hà Nội' },
  { value: 2, label: 'Thành phố Hồ Chí Minh' },
  { value: 3, label: 'Hải Phòng' },
  { value: 4, label: 'Đà Nẵng' },
  { value: 5, label: 'Hà Giang' },
];
