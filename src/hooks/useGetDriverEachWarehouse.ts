import { useState, useEffect } from 'react';
import { getDriverEachWarehouse } from '../api/user';
import { useLoading } from '../components/LoadingProvider';
import { StockKeeperInterface, WarehouseDriver } from '../interface/Auth';

export const useGetDriverEachWarehouse = (user: StockKeeperInterface) => {
  const [drivers, setDrivers] = useState<WarehouseDriver[]>([]);
  const { setLoading } = useLoading();

  useEffect(() => {
    const callGetDriverEachWarehouse = async () => {
      setLoading(true);
      const warehouseDrivers = await getDriverEachWarehouse();
      setLoading(false);
      setDrivers(warehouseDrivers);
    };

    if (user) callGetDriverEachWarehouse();
  }, [setLoading, user]);

  return { drivers };
};
