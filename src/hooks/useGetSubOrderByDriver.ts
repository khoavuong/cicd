import { useEffect, useState } from 'react';
import { getSubOrderByDriver } from '../api/order';
import { useLoading } from '../components/LoadingProvider';
import { useUser } from '../components/UserProvider';
import { SubOrderByDriver } from '../interface/Order';
import { getAccessToken } from '../utils/accessToken';
import { alertError } from '../utils/alert';

export const useGetSubOrderByDriver = () => {
  const { user } = useUser();
  const { setLoading } = useLoading();
  const [subOrders, setSubOrders] = useState<SubOrderByDriver[]>(null);

  useEffect(() => {
    const callGetSubOrderByDriver = async () => {
      const payload = {
        driverId: user.id,
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };
      try {
        setLoading(true);
        const { orders } = await getSubOrderByDriver(payload);
        setSubOrders(orders);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    if (user && !subOrders) callGetSubOrderByDriver();
  }, [setLoading, user, subOrders]);

  return { subOrders, setSubOrders };
};
