import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { DriverOrders } from './modules/driver/DriverOrders';
import { Sender } from './modules/sender/SenderNew';
import { SignIn } from './pages/SignIn';
import { SignUp } from './pages/SignUp';
import { Direct } from './pages/Direct';
import { StockImport } from './modules/stockkeeper/StockImport';
import { TrackingPages } from './pages/TrackingPages';
import { SenderOrders } from './modules/sender/SenderOrders';
import { SenderEdit } from './modules/sender/SenderEdit';
import { Account } from './modules/sender/SenderAccount';
import { SenderNavigation } from './modules/sender/SenderNavigation';
import { DriverNavigation } from './modules/driver/DriverNavigation';
import { DriverAccount } from './modules/driver/DriverAccount';
import { StockNavigation } from './modules/stockkeeper/StockNavigation';
import { StockList } from './modules/stockkeeper/StockList';
import { DriverHistory } from './modules/driver/DriverHistory';
import { LoadingProvider } from './components/LoadingProvider';
import { UserProvider } from './components/UserProvider';
import { AdminNavigation } from './modules/admin/AdminNavigation';
import { AdminAssign } from './modules/admin/AdminAssign';
import { StockExport } from './modules/stockkeeper/StockExport';
import { AdminAddStock } from './modules/admin/AdminAddStock';
import { AdminAddStockKeeper } from './modules/admin/AdminAddStockKeeper';
import { AdminAddDriver } from './modules/admin/AdminAddDriver';
import { StockAccount } from './modules/stockkeeper/StockAccount';
import { SenderPayment } from './modules/sender/SenderPayment';
import { Test } from './components/Test';
import { StockHistory } from './modules/stockkeeper/StockHistory';
import { SenderIssue } from './modules/sender/SenderIssue';
import { AdminIssue } from './modules/admin/AdminIssue';
import { AdminStatistic } from './modules/admin/AdminStatistic';
import { ReceiverSignIn } from './pages/ReceiverSignIn';
import { ReceiverOrders } from './pages/ReceiverOrders';
import { OtpResultProvider } from './components/OtpResultProvider';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <LoadingProvider>
          <UserProvider>
            <Route exact path="/signin" component={SignIn} />
            <Route exact path="/register" component={SignUp} />
            <Route exact path="/" component={Direct} />
            <Route exact path="/tracking" component={TrackingPages} />
            <Route exact path="/blank" component={Test} />
            <OtpResultProvider>
              <Route exact path="/receiver/signin" component={ReceiverSignIn} />
              <Route exact path="/receiver/order" component={ReceiverOrders} />
            </OtpResultProvider>

            <Route path="/stockkeeper">
              <StockNavigation>
                <Route exact path="/stockkeeper/account" component={StockAccount} />
                <Route exact path="/stockkeeper/import" component={StockImport} />
                <Route exact path="/stockkeeper/instock" component={StockList} />
                <Route exact path="/stockkeeper/export" component={StockExport} />
                <Route exact path="/stockkeeper/history" component={StockHistory} />
              </StockNavigation>
            </Route>

            <Route path="/driver">
              <DriverNavigation>
                <Route exact path="/driver/account" component={DriverAccount} />
                <Route exact path="/driver/orders" component={DriverOrders} />
                <Route exact path="/driver/history" component={DriverHistory} />
              </DriverNavigation>
            </Route>

            <Route path="/sender">
              <SenderNavigation>
                <Switch>
                  <Route exact path="/sender/new" component={Sender} />
                  <Route exact path="/sender/account" component={Account} />
                  <Route exact path="/sender/orders" component={SenderOrders} />
                  <Route exact path="/sender/edit" component={SenderEdit} />
                  <Route exact path="/sender/issue" component={SenderIssue} />
                  <Route exact path="/sender/payment" component={SenderPayment} />
                </Switch>
              </SenderNavigation>
            </Route>

            <Route path="/admin">
              <AdminNavigation>
                <Switch>
                  <Route exact path="/admin/assign" component={AdminAssign} />
                  <Route exact path="/admin/addStock" component={AdminAddStock} />
                  <Route exact path="/admin/addStockkeeper" component={AdminAddStockKeeper} />
                  <Route exact path="/admin/addDriver" component={AdminAddDriver} />
                  <Route exact path="/admin/issue" component={AdminIssue} />
                  <Route exact path="/admin/statistic" component={AdminStatistic} />
                </Switch>
              </AdminNavigation>
            </Route>
          </UserProvider>
        </LoadingProvider>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
