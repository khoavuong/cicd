import React, { createContext, ReactNode, useEffect, useState } from 'react';
import { useContext } from 'react';
import { getUser } from '../api/user';
import {
  DriverInterface,
  StockKeeperInterface,
  UserInterface,
} from '../interface/Auth';
import { alertError } from '../utils/alert';
import { useLoading } from './LoadingProvider';

const UserContext =
  createContext<{
    user: UserInterface | DriverInterface | StockKeeperInterface;
    setUser: React.Dispatch<
      React.SetStateAction<
        UserInterface | DriverInterface | StockKeeperInterface
      >
    >;
  }>(null);

interface UserProviderProps {
  children: ReactNode;
}

export const UserProvider = ({ children }: UserProviderProps) => {
  const [user, setUser] =
    useState<UserInterface | DriverInterface | StockKeeperInterface>(null);
  const value = { user, setUser };

  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
};

export const useUser = () => {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error('useUser must be used within a UserProvider');
  }
  return context;
};

export const useGetUser = () => {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error('useUser must be used within a UserProvider');
  }
  const { user, setUser } = context;
  // const { isLoading, setLoading } = useLoading();
  const { setLoading } = useLoading();

  useEffect(() => {
    const callGetUser = async () => {
      try {
        setLoading(true);
        const res = await getUser();
        setUser(res);
        setLoading(false);
        // console.log('useUser', isLoading, Date.now());
      } catch (err) {
        alertError(err.message);
      } finally {
      }
    };

    if (!user) callGetUser();
  }, [setLoading, setUser, user]);

  return context;
};
