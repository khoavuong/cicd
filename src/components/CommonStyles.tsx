import { Button, DatePicker, Input, InputNumber, Layout, Select } from 'antd';
import styled from 'styled-components';

export const FlexSpaceBetween = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const InfoContainer = styled.div`
  color: #00467f;
  font-size: 13px;
`;

export const InfoTitle = styled.h3`
  color: #f26522;
`;

export const Logo = styled.div`
  margin: 30px;
  margin-bottom: 0;
  cursor: pointer;
  color: white;
  text-align: center;

  > img,
  svg {
    width: 100%;
    height: 100%;
    fill: white;
  }
`;

const { Content } = Layout;

export const LayoutContent = styled(Content)`
  background-color: white;
  overflow: auto;
`;

export const Label = styled.label`
  display: block;
  color: #5c6873;
  margin: 2px;
  font-weight: bold;
  font-size: 16px !important;
`;

export const FilterWrapper = styled.div`
  width: 20%;
`;

export const FlexCenter = styled.div`
  display: flex;
  justify-content: center;

  div:not(:last-child) {
    margin-right: 24px;
  }
`;

export const PillInput = styled(Input)`
  border-radius: 20px;
`;

export const PillInputNumber = styled(InputNumber)`
  border-radius: 20px;
`;

export const PillSelect = styled(Select)`
  width: 100%;

  .ant-select-selector {
    border-radius: 20px !important;
  }
`;

export const PillDatePicker = styled(DatePicker)`
  width: 100%;
  border-radius: 20px;
`;

export const PillButton = styled(Button)`
  border-radius: 20px;
`;

export const EditButton = styled(PillButton)`
  width: 100%;
  margin-top: 2rem;
  background-color: #ff6338;
  border: #ff6338;

  :hover {
    background-color: #ff7d59;
  }
`;

export const User = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 1rem;
`;

export const UserName = styled.div`
  display: flex;
  align-items: center;
  color: rgb(255, 199, 0);
`;

export const UserIconWrapper = styled.div`
  background: #fff;
  height: 44px;
  width: 44px;
  border-radius: 50%;
  margin-right: 8px;
  display: -webkit-flex;
  display: flex;
  -webkit-justify-content: center;
  justify-content: center;
  -webkit-align-items: center;
  align-items: center;
`;

export const BlueText = styled.div`
  color: #01477f;
`;
