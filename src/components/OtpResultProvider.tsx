import React, { createContext, ReactNode, useState } from 'react';
import { useContext } from 'react';
import { SubOrderResponse } from '../interface/Order';

const OtpResultContext = createContext<{
  result: Record<string, SubOrderResponse[]>;
  setResult: React.Dispatch<React.SetStateAction<Record<string, SubOrderResponse[]>>>;
}>(null);

interface OtpResultProviderProps {
  children: ReactNode;
}

export const OtpResultProvider = ({ children }: OtpResultProviderProps) => {
  const [result, setResult] = useState<Record<string, SubOrderResponse[]>>(null);
  const value = { result, setResult };

  return <OtpResultContext.Provider value={value}>{children}</OtpResultContext.Provider>;
};

export const useOtpResult = () => {
  const context = useContext(OtpResultContext);
  if (!context) {
    throw new Error('useOtpResult must be used within a OtpResultProvider');
  }
  return context;
};
