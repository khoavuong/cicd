import React, { createContext, ReactNode, useState } from 'react';
import styled from 'styled-components';
import { Spin } from 'antd';
import { useContext } from 'react';

const LoadingContext = createContext<{
  isLoading: boolean;
  setLoading: React.Dispatch<React.SetStateAction<boolean>>;
}>(null);

interface LoadingProviderProps {
  children: ReactNode;
}

export const LoadingProvider = ({ children }: LoadingProviderProps) => {
  const [isLoading, setLoading] = useState(false);
  const displayValue = isLoading ? 'block' : 'none';
  const value = { isLoading, setLoading };

  return (
    <LoadingContext.Provider value={value}>
      <div style={{ display: displayValue }}>
        <LoadingLayer />
        <CenterSpinner>
          <Spin size="large" />
        </CenterSpinner>
      </div>
      {children}
    </LoadingContext.Provider>
  );
};

export const useLoading = () => {
  const context = useContext(LoadingContext);
  if (!context) {
    throw new Error('useLoading must be used within a LoadingProvider');
  }
  return context;
};

const LoadingLayer = styled.div`
  position: fixed;
  top: 0;
  height: 100%;
  width: 100%;
  z-index: 9999;
  background-color: white;
  opacity: 0.7;
`;

const CenterSpinner = styled.div`
  top: 50%;
  left: 50%;
  position: fixed;
  z-index: 9999;
`;
