import { render } from '@testing-library/react';
import React from 'react';
import { EmptyTableContent } from './EmptyTable';
import '@testing-library/jest-dom';

describe('React unit test', () => {
  it('Test display text of empty table', () => {
    const { queryByText } = render(<EmptyTableContent />);
    expect(queryByText('Không có dữ liệu hiển thị')).toBeInTheDocument();
  });

  it('SVG image to be in the empty table', () => {
    const { queryByTestId } = render(<EmptyTableContent />);
    expect(queryByTestId('empty-svg').innerHTML).toBe('empty_table.svg');
  });
});
