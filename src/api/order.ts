import { AxiosResponse } from 'axios';
import {
  CreateOrderPayload,
  OrderResponse,
  FindOrderResponse,
  FindOrderPayload,
  GetOrderByUserPayload,
  GetOrderByUserResponse,
  AcceptOrderPayload,
  UpdateSubOrderPayload,
  GetSuborderByDriverResponse,
  GetSuborderByUserPayload,
  GetSuborderByUserResponse,
  GetSubOrderByStockPayload,
  GetSubOrderByStockResponse,
  ImportExportSubOrdersPayload,
  GetImportExportInfoByStockRequestPayload,
  ImportExportResponses,
  SubmitSupportRequestPayload,
  GetSupportRequestByUserPayload,
  GetSupportRequestResponse,
  GetSupportRequestByAdminPayload,
  ReplySupportRequestPayload,
  RemoveSupportRequestPayload,
  GetDriverHistoryRequestPayload,
  GetDriverHistoryResponse,
  GetSuborderByOrderPayload,
  GetSuborderByOrderResponse,
  CancelOrderPayload,
  StatisticTransRequestPayload,
  StatisticTransRequestResponse,
  GetStatisticFeeResponse,
  AddSubOrderPayload,
  ReceiveSubOrderPayload,
  SendSubOrderIssuePayload,
  OrderBasePayload,
  UpdateSubOrderStatusPayload,
  GetOrderInfoByReceiverPhoneNumberPayload,
  SubmitOtpPayload,
  SubOrderResponse,
} from '../interface/Order';
import { order, supportRequest } from './baseUrl';
import JSONbig from 'json-bigint';

const JSONbigString = JSONbig({ storeAsString: true });

export const createOrder = async (payload: CreateOrderPayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await order.post(
    'order-management/create-order',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getOrderByUser = async (payload: GetOrderByUserPayload) => {
  const res: AxiosResponse<OrderResponse<GetOrderByUserResponse>> = await order.post(
    'order-management/get-order-by-user',
    payload,
    {
      transformResponse: (data) => JSONbigString.parse(data),
    }
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const findOrder = async (payload: FindOrderPayload) => {
  const res: AxiosResponse<OrderResponse<FindOrderResponse>> = await order.post(
    'order-management/find-order',
    payload,
    {
      transformResponse: (data) => JSONbigString.parse(data),
    }
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const acceptOrder = async (payload: AcceptOrderPayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await order.post(
    'order-management/accept-order',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getSubOrderByDriver = async (payload: FindOrderPayload) => {
  const res: AxiosResponse<OrderResponse<GetSuborderByDriverResponse>> = await order.post(
    'order-management/get-suborder-by-driver',
    payload,
    {
      transformResponse: (data) => JSONbigString.parse(data),
    }
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const updateSubOrder = async (payload: UpdateSubOrderPayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await order.post(
    'order-management/update-suborder',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getSubOrderByUser = async (payload: GetSuborderByUserPayload) => {
  const res: AxiosResponse<OrderResponse<GetSuborderByUserResponse>> = await order.post(
    'order-management/get-suborder-by-user',
    payload,
    {
      transformResponse: (data) => {
        return JSONbigString.parse(data);
      },
    }
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const importSubOrders = async (payload: ImportExportSubOrdersPayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await order.post(
    'order-management/import-suborder',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getSubOrderByStock = async (payload: GetSubOrderByStockPayload) => {
  const res: AxiosResponse<OrderResponse<GetSubOrderByStockResponse>> = await order.post(
    'order-management/get-suborder-by-stock',
    payload,
    {
      transformResponse: (data) => JSONbigString.parse(data),
    }
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const exportSubOrders = async (payload: ImportExportSubOrdersPayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await order.post(
    'order-management/export-suborder',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getExportInfoByStockRequest = async (
  payload: GetImportExportInfoByStockRequestPayload
) => {
  const res: AxiosResponse<OrderResponse<ImportExportResponses>> = await order.post(
    'order-management/get-export-info-by-stock',
    payload,
    {
      transformResponse: (data) => JSONbigString.parse(data),
    }
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getImportInfoByStockRequest = async (
  payload: GetImportExportInfoByStockRequestPayload
) => {
  const res: AxiosResponse<OrderResponse<ImportExportResponses>> = await order.post(
    'order-management/get-import-info-by-stock',
    payload,
    {
      transformResponse: (data) => JSONbigString.parse(data),
    }
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const submitSupportRequest = async (payload: SubmitSupportRequestPayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await supportRequest.post(
    'support-service/submit-support-request',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getSupportRequestByUser = async (payload: GetSupportRequestByUserPayload) => {
  const res: AxiosResponse<OrderResponse<GetSupportRequestResponse>> = await supportRequest.post(
    'support-service/get-support-request-by-user',
    payload,
    {
      transformResponse: (data) => JSONbigString.parse(data),
    }
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getSupportRequestByAdmin = async (payload: GetSupportRequestByAdminPayload) => {
  const res: AxiosResponse<OrderResponse<GetSupportRequestResponse>> = await supportRequest.post(
    'support-service/get-support-request-by-admin',
    payload,
    {
      transformResponse: (data) => JSONbigString.parse(data),
    }
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const replySupportRequest = async (payload: ReplySupportRequestPayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await supportRequest.post(
    'support-service/reply-support-request',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const removeSupportRequest = async (payload: RemoveSupportRequestPayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await supportRequest.post(
    'support-service/remove-support-request',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getDriverHistoryRequest = async (payload: GetDriverHistoryRequestPayload) => {
  const res: AxiosResponse<OrderResponse<GetDriverHistoryResponse>> = await order.post(
    'order-management/get-driver-history',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getSuborderByOrder = async (payload: GetSuborderByOrderPayload) => {
  const res: AxiosResponse<OrderResponse<GetSuborderByOrderResponse>> = await order.post(
    'order-management/get-suborder-by-order',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const cancelOrder = async (payload: CancelOrderPayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await order.post(
    'order-management/cancel-order',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const statisticTransRequest = async (payload: StatisticTransRequestPayload) => {
  const res: AxiosResponse<OrderResponse<StatisticTransRequestResponse>> = await order.post(
    'order-management/get-statistic-trans',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getStatisticFee = async (payload: StatisticTransRequestPayload) => {
  const res: AxiosResponse<OrderResponse<GetStatisticFeeResponse>> = await order.post(
    'order-management/get-statistic-fee',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const addSubOrder = async (payload: AddSubOrderPayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await order.post(
    'order-management/add-suborder',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const receiveSubOrder = async (payload: ReceiveSubOrderPayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await order.post(
    'order-management/receive-suborder',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const sendSuborderIssue = async (payload: SendSubOrderIssuePayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await order.post(
    'order-management/send-suborder-issue',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getSuborderIssue = async (payload: OrderBasePayload) => {
  const res: AxiosResponse<OrderResponse<GetDriverHistoryResponse>> = await order.post(
    'order-management/get-suborder-issue',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const updateSubOrderStatus = async (payload: UpdateSubOrderStatusPayload) => {
  const res: AxiosResponse<OrderResponse<any>> = await order.post(
    'order-management/update-suborder-status-by-admin',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const getOrderInfoByReceiverPhoneNumber = async (
  payload: GetOrderInfoByReceiverPhoneNumberPayload
) => {
  const res: AxiosResponse<OrderResponse<any>> = await order.post(
    'order-management/get-order-info-by-receiver-phone-number',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};

export const submitOtp = async (payload: SubmitOtpPayload) => {
  interface SubmitOtpPayload {
    result: Record<string, SubOrderResponse[]>;
  }
  const res: AxiosResponse<OrderResponse<SubmitOtpPayload>> = await order.post(
    'order-management/submit-otp',
    payload
  );
  if (res.data.returnCode !== 1) throw new Error(res.data.returnMessage);
  return res.data.data;
};
