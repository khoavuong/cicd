import axios from 'axios';

export const order = axios.create({
  baseURL: 'https://dungdang.codes/',
});

// export const user = axios.create({
// baseURL: 'https://auth-management-staging.herokuapp.com/',
// });

export const user = axios.create({
  baseURL: 'https://dungdang.codes/auth-management/',
});

export const paymentInstance = axios.create({
  baseURL: 'https://dungdang.codes/payment-management/',
});

export const supportRequest = axios.create({
  baseURL: 'https://dungdang.codes/',
});
