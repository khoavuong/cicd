export interface SignInForm {
  username: string;
  password: string;
}

export interface SignUpPayload {
  email: string;
  phone: string;
  username: string;
  address: string;
  password: string;
  provinceCode: number;
  rePassword: string;
}

export interface AdminAddStockKeeperPayload extends SignUpPayload {
  areaWarehouseId: string;
}

export interface AdminAddDriverPayload extends SignUpPayload {
  role: DriverRole;
  vehicleId: string;
  area: string; // wareHouseId
}

export interface UserInterface {
  id: string;
  token: string;
  email: string;
  phone: string;
  username: string;
  address: string;
  role: string;
  provinceCode: number;
}

export interface DriverInterface extends UserInterface {
  status: number;
  vehicleResponse: Vehicle;
  warehouseResponse: WareHouse;
}

export interface StockKeeperInterface extends UserInterface {
  warehouseResponse: WareHouse;
}

export interface UpdateInfoUserPayload {
  email: string;
  phone: string;
  username: string;
  address: string;
}

export interface ChangePasswordPayload {
  oldPassword: string;
  newPassword: string;
}

export interface GetTransportationCostPayload {
  isDirectlySend: number;
  isDirectlyReceive: number;
  addressReceiver: string;
  addressReceiverCode: number;
  weight: number;
}

export interface GetTransportationCostResponse {
  cost: string;
  srcStockId: string;
  destStockId: string;
  addressDestWarehouse: string;
  addressSrcWarehouse: string;
}

export interface WarehouseDriver {
  phone: string;
  username: string;
  driverEntity: {
    id: string;
    area: string;
    status: number;
    vehicleId: string;
  };
}

export interface AddAddressWarehousePayload {
  areaCode: number;
  address: string;
}
export interface GetListDriverResponse {
  id: string;
  username: string;
  phone: string;
  role: DriverRole;
  email: string;
  vehicleResponse: Vehicle;
  warehouseResponse: WareHouse;
}

export interface GetListStockerResponse {
  id: string;
  username: string;
  email: string;
  warehouseResponse: WareHouse;
}

export interface Vehicle {
  id: string;
  totalWeight: number;
  currentWeight: number;
}

export interface WareHouse {
  id: string;
  address: string;
  areaCode: number;
}

export enum DriverRole {
  Internal = 'ROLE_DRIVER_INTER',
  External = 'ROLE_DRIVER_EXTER',
}

export const DriverRoleText = {
  [DriverRole.External]: 'Liên tỉnh',
  [DriverRole.Internal]: 'Nội tỉnh',
};

export type PaymentMethod = 'ATM' | 'CC' | 'ZALOPAY';

export interface PaymentPayload {
  typePayment: PaymentMethod;
  amount: number;
  description: string;
  item: string;
  embed_data: string;
  codeOrders: string;
}

export interface PaymentResponse {
  return_code: number;
  return_message: string;
  order_url: string;
}
