export interface OrderResponse<T> {
  returnCode: number;
  returnMessage: string;
  data: T;
}

export interface OrderBasePayload {
  reqTime: number;
  accessToken: string;
}

export type PaymentSide = 'sender' | 'receiver';

export type SubOrderStatusNumber = -1 | 1 | 2 | 3 | 4 | 5 | 6 | 7;
export type OrderStatusNumber = -3 | -1 | 1 | 2 | 3;

export enum SuborderStatus {
  FAILED = -1,
  REQUESTED = 1,
  COMING_SEND_STOCK = 2,
  ARRIVED_SEND_STOCK = 3,
  TRANSFERING_ARRIVED_STOCK = 4,
  ARRIVED_DEST_STOCK = 5,
  TRANSFERING_TO_RECEIVER = 6,
  DONE = 7,
}

export enum OrderStatus {
  CANCELED = -3,
  FAILEd = -1,
  PROCESSING = 1,
  DELIVERING = 2,
  DONE = 3,
}

export enum PaymentStatus {
  WAITING = 1,
  PAID = 2,
}

export enum OrderStatusText {
  'Yêu cầu thất bại' = -1,
  'Đang được tiếp nhận' = 1,
  'Đang vận chuyển' = 2,
  'Hoàn thành - Chưa thanh toán' = 3,
  'Hoàn thành - Đã Thanh toán' = 4,
}

export const SuborderStatusText = {
  [SuborderStatus.FAILED]: 'Đơn hàng hỏng / thất lạc',
  [SuborderStatus.REQUESTED]: 'Đang đến lấy hàng',
  [SuborderStatus.COMING_SEND_STOCK]: 'Đang đến kho gửi',
  [SuborderStatus.ARRIVED_SEND_STOCK]: 'Đã vào kho gửi',
  [SuborderStatus.TRANSFERING_ARRIVED_STOCK]: 'Đang giao liên tỉnh',
  [SuborderStatus.ARRIVED_DEST_STOCK]: 'Đã vào kho đến',
  [SuborderStatus.TRANSFERING_TO_RECEIVER]: 'Đang giao người nhận',
  [SuborderStatus.DONE]: 'Hoàn tất',
};

export interface CreateOrderPayload extends OrderBasePayload {
  paymentSide: PaymentSide;

  senderId: string;
  nameSender: string;
  phoneSender: string;
  emailSender: string;
  fromAddress: string;
  phoneReceiver: string;
  emailReceiver: string;
  nameReceiver: string;
  toAddress: string;
  description: string;
  fee: number;
  kind: string;
  weight: number;
  value: number;

  provinceCode: number;
  srcStockId: string;
  destStockId: string;
  addressDestWarehouse: string;
  addressSrcWarehouse: string;
}

export interface GetOrderByUserPayload extends OrderBasePayload {
  userId: string;
}

export interface OrderByUser {
  detailOrder: string;
  userId: string;
  orderTime: number;
  orderId: string;
  value: number;
  description: string;
  orderStatus: OrderStatus;
  fee: number;
  paymentSide: PaymentSide;
  totalWeight: number;
  remainWeight: number;
  driverIds: string;
  assignedDrivers: string;
  destStockId: string;
  srcStockId: string;
  addressDestWarehouse: string;
  addressSrcWarehouse: string;
  paymentStatus: PaymentStatus;
  directlyReceive: boolean;
  directlySend: boolean;
}

export interface GetOrderByUserResponse {
  orders: OrderByUser[];
}

export interface FindOrderPayload extends OrderBasePayload {
  driverId: string | string;
}

export interface FoundOrder {
  orderId: string;
  nameSender: string;
  phoneSender: string;
  addressSender: string;
  kind: string;
  totalWeight: number;
  remainWeight: number;
}

export interface FindOrderResponse {
  orders: FoundOrder[];
}

export interface AcceptOrderPayload extends OrderBasePayload {
  driverId: string;
  orderId: string;
  driverName: string;
  driverPhone: string;
}

export interface UpdateSubOrderPayload extends OrderBasePayload {
  subOrderId: string;
  actualWeight: number;
}

export interface AddSubOrderPayload extends OrderBasePayload {
  orderId: string;
  weight: number;
  stockKeeperId: string;
  stockId: string;
}

export interface SubOrderByDriver {
  srcStockId: string;
  destStockId: string;
  subOrderId: string;
  orderId: string;
  detailOrder: string;
  totalWeight: number;
  remainWeight: number;
  weight: number;
  reqTime: number;
  orderStatus: SubOrderStatusNumber;
}

export interface GetSuborderByDriverResponse {
  orders: SubOrderByDriver[];
}

export interface GetSuborderByUserPayload extends OrderBasePayload {
  userId: string;
}

export interface SubOrderByUser {
  detailOrder: string;
  orderId: string;
  orderStatus: SubOrderStatusNumber;
  reqTime: number;
  subOrderId: string;
  totalWeight: number;
  weight: number;
  driverId: string;
  externalDriverId: string;
  destDriverId: string;
  driverName: string;
  driverPhone: string;
  destDriverName: string;
  destDriverPhone: string;
  externalDriverName: string;
  externalDriverPhone: string;
}

export interface GetSuborderByUserResponse {
  orders: SubOrderByUser[];
}

export interface ImportExportSubOrdersPayload extends OrderBasePayload {
  subOrderIds: string[];
  stockKeeperId: string;
  stockId: string;
  driverId: string;

  // New
  stockKeeperName: string;
  driverName: string;
  stockKeeperPhone: string;
  driverPhone: string;
}

export interface GetSubOrderByStockPayload extends OrderBasePayload {
  stockId: string;
}

export interface SubOrderResponse {
  detailOrder: string;
  orderId: string;
  orderStatus: SubOrderStatusNumber;
  reqTime: number;
  subOrderId: string;
  totalWeight: number;
  remainWeight: number;
  weight: number;
  driverId: string;
  description: string;
  driverName: string;
  driverPhone: string;
  addressSrcWarehouse: string;
  addressDestWarehouse: string;
  externalDriverId: string;
  externalDriverName: string;
  externalDriverPhone: string;
  destDriverId: string;
  destDriverName: string;
  destDriverPhone: string;
  srcStockId: string;
  destStockId: string;
  issue: string;
  timeHistory: SubOrderTimeHistory[];
}

export interface SubOrderTimeHistory {
  time: number;
  subOrderStatus: SubOrderStatusNumber;
}

export interface GetSubOrderByStockResponse {
  orders: SubOrderResponse[];
}

export interface ImportExportResponses {
  importExportResponses: ImportExportEntity[];
}

export interface ImportExportEntity {
  driverId: string;
  id: string;
  stockkeeperId: string;
  time: number;
  subOrderResponses: SubOrderResponse[];
  stockKeeperPhone: string;
  stockKeeperName: string;
  driverPhone: string;
  driverName: string;
}

export interface GetImportExportInfoByStockRequestPayload extends OrderBasePayload {
  stockId: string;
}

export interface SubmitSupportRequestPayload extends OrderBasePayload {
  orderId: string;
  content: string;
  userId: string;
  emailAddress: string;
}

export interface ReplySupportRequestPayload extends OrderBasePayload {
  orderId: string;
  reply: string;
}

export interface RemoveSupportRequestPayload extends OrderBasePayload {
  orderId: string;
  userId: string;
}

export interface GetSupportRequestByUserPayload extends OrderBasePayload {
  userId: string;
}

export interface GetSupportRequestByAdminPayload extends OrderBasePayload {}

export interface GetSupportRequestResponse {
  supportRequests: SupportRequest[];
}

export interface SupportRequest {
  content: string;
  orderId: string;
  reqTime: number;
  userId: string;
  reply?: string;
}

export interface GetDriverHistoryRequestPayload extends OrderBasePayload {
  driverId: string;
}

export interface GetOrderInfoByReceiverPhoneNumberPayload {
  reqTime: number;
  receiverPhoneNumber: string;
}

export interface SubmitOtpPayload {
  otp: number;
  phoneNumber: string;
  type: number;
}

export interface GetDriverHistoryResponse {
  subOrderResponses: SubOrderResponse[];
}

export interface DetailOrder {
  nameReceiver: string;
  phoneReceiver: string;
  emailSender: string;
  emailReceiver: string;
  toAddress: string;
  nameSender: string;
  phoneSender: string;
  fromAddress: string;
  kind: string;
}

export interface GetSuborderByOrderPayload extends OrderBasePayload {
  orderId: string;
}

export interface ReceiveSubOrderPayload extends OrderBasePayload {
  subOrderIds: string[];
  stockKeeperId: string;
  stockId: string;
}

export interface UpdateSubOrderStatusPayload extends OrderBasePayload {
  subOrderId: string;
  newStatus: SuborderStatus;
}

export interface GetSuborderByOrderResponse {
  subOrderResponses: SubOrderResponse[];
}

export interface CancelOrderPayload extends OrderBasePayload {
  orderId: string;
  reason: string;
}

export interface StatisticTransRequestPayload extends OrderBasePayload {
  fromDate: string;
  toDate: string;
}

export interface StatisticTransRequestResponse {
  statisticTransList: StatisticTransObject[];
}

export interface GetStatisticFeeResponse {
  statisticFees: StatisticFeeObject[];
}

export interface StatisticFeeObject {
  date: number;
  totalFee: number;
}

export interface StatisticTransObject {
  date: number;
  transSuccess: number;
  transCancel: number;
  transFail: number;
}

export interface SendSubOrderIssuePayload extends OrderBasePayload {
  subOrderId: string;
  driverId: string;
  issue: string;
}
