import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Input, Row, Table, Col } from 'antd';
import { useHistory } from 'react-router';
import { SearchInfoPanel } from '../components/SearchInfoPanel';
import { alertError } from '../utils/alert';
import { useLoading } from '../components/LoadingProvider';
import { getSuborderByOrder } from '../api/order';
import { getAccessToken } from '../utils/accessToken';
import {
  DetailOrder,
  SubOrderResponse,
  SuborderStatusText,
  SubOrderTimeHistory,
} from '../interface/Order';
import { BlueText, FilterWrapper, FlexCenter, Logo, PillSelect } from '../components/CommonStyles';
import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  FieldNumberOutlined,
  CodeSandboxOutlined,
} from '@ant-design/icons';
import { ReactComponent as LogoImg } from '../assets/logo.svg';
import { ReactComponent as WeightIcon } from '../assets/weight.svg';

export const TrackingPages = () => {
  const { setLoading } = useLoading();
  const history = useHistory();
  const [searchId, setSearchId] = useState('');
  const [subOrderResponses, setSubOrderResponses] = useState<SubOrderResponse[]>([]);
  const [selectedSubOrderResponse, setSelectedSubOrderResponse] = useState<SubOrderResponse>(null);

  const urlParams = new URLSearchParams(window.location.search);

  const onSearch = async (value: string) => {
    try {
      history.push(`/tracking?order_code=${value}`);
      setSearchId(value);
      const payload = {
        accessToken: getAccessToken(),
        reqTime: Date.now(),
        orderId: value,
      };
      setLoading(true);
      const { subOrderResponses } = await getSuborderByOrder(payload);
      const tableSubOrderResponses = subOrderResponses.map((item) => ({
        ...item,
        timeHistory: item.timeHistory
          .map((time) => ({
            ...time,
            key: time.subOrderStatus,
          }))
          .reverse(),
      }));
      setSubOrderResponses(tableSubOrderResponses);
      setSelectedSubOrderResponse(tableSubOrderResponses[0]);
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    setSearchId(urlParams.get('order_code'));
    onSearch(urlParams.get('order_code'));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSelectSubOrderResponse = (value: string) => {
    setSelectedSubOrderResponse(subOrderResponses.find((item) => item.subOrderId === value));
  };

  const renderContent = () => {
    if (selectedSubOrderResponse) {
      const {
        nameReceiver,
        nameSender,
        phoneReceiver,
        phoneSender,
        toAddress,
        fromAddress,
        emailReceiver,
        emailSender,
        kind,
      }: DetailOrder = JSON.parse(selectedSubOrderResponse.detailOrder);

      return (
        <>
          <PanelContainer>
            <SearchInfoPanel title="THÔNG TIN YÊU CẦU">
              <p>
                <FieldNumberOutlined /> <strong>{urlParams.get('order_code')}</strong>
              </p>
              <p>
                <WeightIcon />{' '}
                <strong>{selectedSubOrderResponse.totalWeight.toLocaleString()}kg</strong>
              </p>
              <p>
                <CodeSandboxOutlined /> <strong>{kind}</strong>
              </p>
            </SearchInfoPanel>
            <SearchInfoPanel title="NGƯỜI GỬI">
              <p>
                <UserOutlined /> {nameSender}
              </p>
              <p>
                <PhoneOutlined /> {phoneSender}
              </p>
              <p>
                <MailOutlined /> {emailSender}
              </p>
              <p>
                <HomeOutlined /> {fromAddress}
              </p>
            </SearchInfoPanel>
            <SearchInfoPanel title="NGƯỜI NHẬN">
              <p>
                <UserOutlined /> {nameReceiver}
              </p>
              <p>
                <PhoneOutlined /> {phoneReceiver}
              </p>
              <p>
                <MailOutlined /> {emailReceiver}
              </p>
              <p>
                <HomeOutlined /> {toAddress}
              </p>
            </SearchInfoPanel>
          </PanelContainer>
          <hr />
          <HistoryTable>
            <FlexCenter>
              <FilterWrapper>
                <PillSelect
                  value={selectedSubOrderResponse?.subOrderId}
                  placeholder="Chọn đơn hàng"
                  onChange={onSelectSubOrderResponse}
                >
                  {subOrderResponses.map((item) => (
                    <Option key={item.subOrderId} value={item.subOrderId}>
                      {item.subOrderId}
                    </Option>
                  ))}
                </PillSelect>
              </FilterWrapper>
            </FlexCenter>
            <br />
            <Table
              bordered
              columns={columns}
              pagination={{ pageSize: 7, hideOnSinglePage: true }}
              dataSource={selectedSubOrderResponse.timeHistory}
            />
          </HistoryTable>
        </>
      );
    } else {
      return (
        <>
          <br />
          <EmptyContainer>
            <svg
              width="300"
              height="300"
              viewBox="0 0 300 300"
              fill="none"
              className="empty-state-icon"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M0.268694 128.784C-3.19641 80.3388 27.1467 36.0407 74.2982 35.014C121.45 33.9873 125.369 89.724 162.648 91.7636C199.927 93.8033 255.61 57.7541 289.862 106.388C324.115 155.023 269.763 258 154.144 258C38.5257 258 3.73379 177.228 0.268694 128.784Z"
                fill="#F5F5F5"
              ></path>
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M153.796 257.931C200.629 257.931 237.412 241.052 262.331 217.734C259.695 204.654 255.555 191.081 250.064 177.491C228.831 124.945 190.472 119.861 148.515 122.269C117.857 124.029 94.7436 131.944 77.8005 143.483C8.79337 182.543 5.60775 252.428 38.1399 262.446C59.4009 268.993 78.6492 256.938 89.5069 247.711C107.477 254.141 128.739 257.931 153.796 257.931Z"
                fill="#D9D9D9"
              ></path>
              <mask
                id="mask0"
                mask-type="alpha"
                maskUnits="userSpaceOnUse"
                x="18"
                y="121"
                width="245"
                height="144"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M153.796 257.931C200.629 257.931 237.412 241.052 262.331 217.734C259.694 204.654 255.555 191.081 250.063 177.491C228.831 124.945 190.472 119.861 148.515 122.269C117.857 124.029 94.7436 131.944 77.8005 143.483C8.79333 182.543 5.60771 252.428 38.1399 262.446C59.4009 268.993 78.6491 256.938 89.5069 247.711C107.477 254.141 128.739 257.931 153.796 257.931Z"
                  fill="white"
                ></path>
              </mask>
              <g mask="url(#mask0)">
                <path
                  d="M114.34 144.201C113.292 156.253 114.34 154.233 114.34 167.034C114.34 174.527 118.333 195.072 107.615 220.968C102.545 233.217 95.248 244.556 88.7878 250.835"
                  stroke="white"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="bevel"
                ></path>
              </g>
              <path
                d="M84.4666 179.361C89.9617 170.958 94.8212 164.644 95.1632 160.05C95.6799 153.11 95.6811 147.933 97.0844 143.518"
                stroke="white"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="bevel"
              ></path>
              <path
                d="M109.008 137.067C109.008 137.067 103.526 141.413 102.893 151.334"
                stroke="white"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="bevel"
              ></path>
              <path
                d="M105.215 135.803C105.215 135.803 99.7331 140.149 99.0997 150.07"
                stroke="white"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="bevel"
              ></path>
              <path
                d="M110.698 140.63C110.698 140.63 108.472 142.727 108.472 153.578"
                stroke="white"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="bevel"
              ></path>
              <path
                d="M114.119 117.29L110.236 116.847C111.982 113.802 111.367 109.186 108.392 102.998"
                stroke="#D9D9D9"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              ></path>
              <path
                d="M94.0633 96.3809C96.1601 93.5475 99.1815 92.1952 103.127 92.3238"
                stroke="#D9D9D9"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              ></path>
              <path
                d="M114.771 97.3682C116.868 94.5349 119.889 93.1825 123.835 93.3111"
                stroke="#D9D9D9"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              ></path>
              <path
                d="M116.035 103.69C118.132 100.856 121.154 99.5038 125.099 99.6324"
                stroke="#D9D9D9"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              ></path>
              <path
                d="M125.411 137.194C124.432 136.053 123.251 135.077 121.87 134.265C115.864 130.734 105.744 130.792 101.348 122.211C96.9526 113.631 102.419 117.583 92.4982 97.9059C88.4116 89.8008 88.873 82.096 93.8826 74.7915V74.7915"
                stroke="#D9D9D9"
                strokeWidth="1.5"
              ></path>
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M103.851 110.545C102.887 110.954 101.552 109.983 100.87 108.377C100.188 106.77 100.417 105.135 101.381 104.726C102.345 104.317 103.679 105.288 104.361 106.895C105.043 108.501 104.815 110.136 103.851 110.545Z"
                fill="#D9D9D9"
              ></path>
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M88.4242 85.1802C84.7174 83.6706 77.4693 73.4464 86.4745 65.7059C96.2122 57.3359 128.822 53.2719 140.524 64.4812C151.983 75.4574 146.854 103.398 140.524 110.176C138.668 112.163 138.738 110.688 140.24 107.753C143.012 102.335 143.316 96.9567 138.694 96.7637C137.214 96.7019 137.339 108.202 136.597 108.987C135.98 109.64 134.226 108.928 133.296 106.501C132.367 104.073 131.942 96.1594 131.307 93.1035C129.32 83.5291 125.483 74.0168 119.277 78.199C101.306 90.3076 92.1309 86.6897 88.4242 85.1802Z"
                fill="#D9D9D9"
              ></path>
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M108 125C108 125 108 120.769 113.865 117.095C119.731 113.422 133 116.189 133 116.189L108 125Z"
                fill="#D9D9D9"
              ></path>
              <path
                d="M96.1984 126.993L101.306 141.026"
                stroke="#D9D9D9"
                strokeWidth="3"
                strokeLinecap="round"
                strokeLinejoin="bevel"
              ></path>
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M96.1985 126.993C104.072 124.127 108.132 115.421 105.266 107.548C102.4 99.6744 93.6943 95.6148 85.8208 98.4805C77.9474 101.346 73.8878 110.052 76.7535 117.925C79.6192 125.799 88.325 129.859 96.1985 126.993Z"
                stroke="#D9D9D9"
                strokeWidth="3"
                strokeLinecap="round"
                strokeLinejoin="bevel"
              ></path>
            </svg>

            <EmptyOrderId>Mã yêu cầu không tồn tại</EmptyOrderId>
            <div>Chúng tôi không tìm thấy mã đơn hàng của bạn trong hệ thống.</div>
            <div>Vui lòng kiểm tra lại mã đơn hàng.</div>
          </EmptyContainer>
        </>
      );
    }
  };

  return (
    <>
      <Container>
        <NavBar>
          <NavBarContent>
            <Col>
              <NavBarLogo onClick={() => history.push('/receiver/signin')}>
                <LogoImg />
              </NavBarLogo>
            </Col>

            <Col offset={4} style={{ display: 'flex', alignItems: 'center' }}>
              <SearchOrder
                value={searchId}
                placeholder="Nhập mã yêu cầu"
                onChange={(e) => {
                  setSearchId(e.target.value);
                }}
                onSearch={onSearch}
              />
            </Col>
          </NavBarContent>
        </NavBar>
      </Container>

      {renderContent()}
    </>
  );
};

const columns = [
  {
    title: 'Trạng thái',
    render: (_: any, timeHistory: SubOrderTimeHistory) => {
      const { subOrderStatus } = timeHistory;
      return (
        <BlueText>
          <p>{SuborderStatusText[subOrderStatus]}</p>
        </BlueText>
      );
    },
  },
  {
    title: 'Thời gian',
    render: (_: any, timeHistory: SubOrderTimeHistory) => {
      const { time } = timeHistory;
      return (
        <BlueText>
          <p>{new Date(time).toLocaleString()}</p>
        </BlueText>
      );
    },
  },
];

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const PanelContainer = styled.div`
  display: flex;
  justify-content: space-around;
  width: 75%;
  margin: auto;
  margin-top: 3rem;
`;

const SearchOrder = styled(Input.Search)`
  width: 300px;
  display: flex;
  align-items: center;
`;

const HistoryTable = styled.div`
  margin: auto;
  max-width: 60%;
  margin-top: 25px;
  margin-bottom: 25px;
`;

const EmptyContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 2rem;
  flex-direction: column;
`;

const EmptyOrderId = styled.div`
  font-size: 28px;
  line-height: 38px;
  color: #f26522;
  font-weight: 600;
  margin-bottom: 20px;
`;

const NavBar = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  background-color: #e9e9e9;
`;

const NavBarContent = styled(Row)`
  width: 700px;
`;

const NavBarLogo = styled(Logo)`
  margin: 0;
  width: 70px;
  height: 70px;

  > img,
  svg {
    width: 100%;
    height: 100%;
    fill: white;
  }
`;

const { Option } = PillSelect;
