import React from 'react';
import styled from 'styled-components';
import { Form } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { signIn } from '../api/user';
import { SignInForm } from '../interface/Auth';
import { useLoading } from '../components/LoadingProvider';
import { alertError } from '../utils/alert';
import { FormInstance } from 'antd/lib/form';
import { EditButton, Label, PillInput } from '../components/CommonStyles';
import { useUser } from '../components/UserProvider';
import Particles from 'react-particles-js';
import bgImg from '../assets/login_bg.jpg';
import ReactGA from 'react-ga';

export const SignIn = () => {
  const { setUser } = useUser();
  const history = useHistory();
  const { setLoading } = useLoading();
  const [form]: FormInstance<SignInForm>[] = Form.useForm();

  const onFinish = async () => {
    try {
      setLoading(true);
      const userData = await signIn(form.getFieldsValue());
      ReactGA.event({
        category: 'User',
        action: 'User sign in',
      });
      setUser(userData);
      setLoading(false);
      switch (userData.role) {
        case 'ROLE_USER':
          history.push('/sender/orders');
          break;
        case 'ROLE_DRIVER_INTER':
          history.push('/driver/orders');
          break;
        case 'ROLE_STOCKER':
          history.push('/stockkeeper/import');
          break;
        case 'ROLE_ADMIN':
          history.push('/admin/assign');
          break;
        default:
          history.push('/');
          break;
      }
    } catch (err) {
      alertError(err.response.data.message || err.message);
      form.resetFields();
      setLoading(false);
    }
  };

  return (
    <Wrapper>
      <BackgroundParticles
        params={{
          particles: {
            color: {
              value: ['#BD10E0', '#B8E986', '#50E3C2', '#FFD300', '#E86363'],
            },
            number: {
              value: 160,
              density: {
                enable: false,
              },
            },
            size: {
              value: 3,
              random: true,
              anim: {
                speed: 4,
                size_min: 0.3,
              },
            },
            line_linked: {
              enable: false,
            },
            move: {
              random: true,
              speed: 1,
              direction: 'top',
              out_mode: 'out',
            },
          },
          interactivity: {
            modes: {
              bubble: {
                distance: 250,
                duration: 2,
                size: 0,
                opacity: 0,
              },
              repulse: {
                distance: 400,
                duration: 4,
              },
            },
          },
        }}
      />

      <ImageWrapper>
        <SideImage src={bgImg} alt="bgImage" />
      </ImageWrapper>

      <Container>
        <FormWrapper>
          <Title>Đăng nhập</Title>
          <Form name="normal_login" onFinish={onFinish} form={form}>
            <Label>Tài khoản</Label>
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập số điện thoại/email',
                },
              ]}
            >
              <PillInput
                size="large"
                prefix={<UserOutlined />}
                placeholder="Nhập số điện thoại/email"
              />
            </Form.Item>

            <Label>Mật khẩu</Label>
            <Form.Item
              name="password"
              rules={[{ required: true, message: 'Vui lòng nhập mật khẩu của bạn' }]}
            >
              <PillInput
                size="large"
                prefix={<LockOutlined />}
                type="password"
                placeholder="Nhập mật khẩu"
              />
            </Form.Item>

            <Form.Item>
              <EditButton size="large" type="primary" htmlType="submit">
                Đăng nhập
              </EditButton>
            </Form.Item>
          </Form>

          <ToRegister>
            Bạn chưa có tài khoản <Link to="/register">Đăng ký ngay</Link>
          </ToRegister>

          <ToRegister>
            Hoặc đến <Link to="/receiver/signin">trang</Link> tra cứu mã yêu cầu / đăng nhập cho
            người nhận bằng số điện thoại
          </ToRegister>
        </FormWrapper>
      </Container>
    </Wrapper>
  );
};

const Container = styled.div`
  width: 60%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const FormWrapper = styled.div`
  width: 500px;
`;

const Title = styled.div`
  font-size: 24px;
  text-align: center;
`;

const ToRegister = styled.h3`
  text-align: center;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  height: 100%;
`;

const ImageWrapper = styled.div`
  background-image: linear-gradient(180deg, #aaaafe 12%, #6653ff 90%);
  width: 40%;
`;

const SideImage = styled.img`
  height: 100%;
  width: 100%;
`;

const BackgroundParticles = styled(Particles)`
  position: fixed !important;
  right: 0;
  top: 0;
  width: 60%;
  height: 100%;
  z-index: -1;
`;
