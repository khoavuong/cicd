import React, { useState } from 'react';
import styled from 'styled-components';
import { Row, Table, Col } from 'antd';
import { useHistory } from 'react-router';
import { SearchInfoPanel } from '../components/SearchInfoPanel';
import {
  DetailOrder,
  SubOrderResponse,
  SuborderStatusText,
  SubOrderTimeHistory,
} from '../interface/Order';
import { BlueText, FilterWrapper, FlexCenter, Logo, PillSelect } from '../components/CommonStyles';
import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  FieldNumberOutlined,
  CodeSandboxOutlined,
} from '@ant-design/icons';
import { ReactComponent as LogoImg } from '../assets/logo.svg';
import { ReactComponent as WeightIcon } from '../assets/weight.svg';
// import { mockOtpResult } from '../constants/mockTableData';
import { useOtpResult } from '../components/OtpResultProvider';

export const ReceiverOrders = () => {
  const { result } = useOtpResult();
  // const result = mockOtpResult;
  const orders = Object.entries(result).map((data) => ({
    orderId: data[0],
    subOrderResponses: data[1],
  }));

  const history = useHistory();
  const [selectedOrder, setSelectedOrder] = useState(orders[0]);
  const { subOrderResponses } = selectedOrder;
  const [selectedSubOrderResponse, setSelectedSubOrderResponse] = useState<SubOrderResponse>(
    subOrderResponses[0]
  );

  const onSelectSubOrderResponse = (value: string) => {
    setSelectedSubOrderResponse(subOrderResponses.find((item) => item.subOrderId === value));
  };

  const onSelectOrderResponse = (value: string) => {
    setSelectedOrder(orders.find((item) => item.orderId === value));
  };

  const renderContent = () => {
    if (selectedSubOrderResponse) {
      const {
        nameReceiver,
        nameSender,
        phoneReceiver,
        phoneSender,
        toAddress,
        fromAddress,
        emailReceiver,
        emailSender,
        kind,
      }: DetailOrder = JSON.parse(selectedSubOrderResponse.detailOrder);

      return (
        <>
          <PanelContainer>
            <SearchInfoPanel title="THÔNG TIN YÊU CẦU">
              <p>
                <FieldNumberOutlined /> <strong>{selectedOrder.orderId}</strong>
              </p>
              <p>
                <WeightIcon />{' '}
                <strong>{selectedSubOrderResponse.totalWeight.toLocaleString()}kg</strong>
              </p>
              <p>
                <CodeSandboxOutlined /> <strong>{kind}</strong>
              </p>
            </SearchInfoPanel>
            <SearchInfoPanel title="NGƯỜI GỬI">
              <p>
                <UserOutlined /> {nameSender}
              </p>
              <p>
                <PhoneOutlined /> {phoneSender}
              </p>
              <p>
                <MailOutlined /> {emailSender}
              </p>
              <p>
                <HomeOutlined /> {fromAddress}
              </p>
            </SearchInfoPanel>
            <SearchInfoPanel title="NGƯỜI NHẬN">
              <p>
                <UserOutlined /> {nameReceiver}
              </p>
              <p>
                <PhoneOutlined /> {phoneReceiver}
              </p>
              <p>
                <MailOutlined /> {emailReceiver}
              </p>
              <p>
                <HomeOutlined /> {toAddress}
              </p>
            </SearchInfoPanel>
          </PanelContainer>
          <hr />
          <HistoryTable>
            <FlexCenter>
              <FilterWrapper>
                <PillSelect
                  value={selectedSubOrderResponse?.subOrderId}
                  placeholder="Chọn đơn hàng"
                  onChange={onSelectSubOrderResponse}
                >
                  {subOrderResponses.map((item) => (
                    <Option key={item.subOrderId} value={item.subOrderId}>
                      {item.subOrderId}
                    </Option>
                  ))}
                </PillSelect>
              </FilterWrapper>
            </FlexCenter>
            <br />
            <Table
              bordered
              columns={columns}
              pagination={{ pageSize: 7, hideOnSinglePage: true }}
              dataSource={selectedSubOrderResponse.timeHistory}
            />
          </HistoryTable>
        </>
      );
    }
  };

  return (
    <>
      <Container>
        <NavBar>
          <NavBarContent>
            <Col>
              <NavBarLogo onClick={() => history.push('/receiver/signin')}>
                <LogoImg />
              </NavBarLogo>
            </Col>

            <Col
              offset={6}
              style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}
            >
              <div style={{ marginTop: '10px', width: '120px' }}>Chọn mã yêu cầu</div>
              <PillSelect
                value={selectedOrder.orderId}
                placeholder="Chọn đơn hàng"
                onChange={onSelectOrderResponse}
              >
                {orders.map((item) => (
                  <Option key={item.orderId} value={item.orderId}>
                    {item.orderId}
                  </Option>
                ))}
              </PillSelect>
            </Col>
          </NavBarContent>
        </NavBar>
      </Container>

      {renderContent()}
    </>
  );
};

const columns = [
  {
    title: 'Trạng thái',
    render: (_: any, timeHistory: SubOrderTimeHistory) => {
      const { subOrderStatus } = timeHistory;
      return (
        <BlueText>
          <p>{SuborderStatusText[subOrderStatus]}</p>
        </BlueText>
      );
    },
  },
  {
    title: 'Thời gian',
    render: (_: any, timeHistory: SubOrderTimeHistory) => {
      const { time } = timeHistory;
      return (
        <BlueText>
          <p>{new Date(time).toLocaleString()}</p>
        </BlueText>
      );
    },
  },
];

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const PanelContainer = styled.div`
  display: flex;
  justify-content: space-around;
  width: 75%;
  margin: auto;
  margin-top: 3rem;
`;

const HistoryTable = styled.div`
  margin: auto;
  max-width: 60%;
  margin-top: 25px;
  margin-bottom: 25px;
`;

const NavBar = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  background-color: #e9e9e9;
`;

const NavBarContent = styled(Row)`
  width: 700px;
`;

const NavBarLogo = styled(Logo)`
  margin: 0;
  width: 80px;
  height: 80px;

  > img,
  svg {
    width: 100%;
    height: 100%;
    fill: white;
  }
`;

const { Option } = PillSelect;
