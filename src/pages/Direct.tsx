import React /* , { useEffect, useState } */ from 'react';
import styled from 'styled-components';
import { Button, Input } from 'antd';
import { useHistory } from 'react-router';

export const Direct = () => {
  const history = useHistory();
  const onSearch = (value: any) => {
    history.push(`/tracking?order_code=${value}`);
  };

  const sendSentryError = () => {
    const foo: any = 4;
    console.log(foo.a.b);
  };

  return (
    <>
      <Container>
        <SearchOrder placeholder="Tracking ID" onSearch={onSearch} />
      </Container>

      <Container>
        <MarginButton type="primary" onClick={() => history.push('/sender/orders')}>
          Sender
        </MarginButton>
        <MarginButton type="primary" onClick={() => history.push('/driver/orders')}>
          Driver
        </MarginButton>
        <MarginButton type="primary" onClick={() => history.push('/stockkeeper/import')}>
          Stock Keeper
        </MarginButton>
        <MarginButton type="primary" onClick={() => history.push('/admin/assign')}>
          Admin
        </MarginButton>
        <MarginButton type="primary" onClick={() => history.push('/blank')}>
          Blank Page
        </MarginButton>
        <MarginButton type="primary" onClick={() => history.push('/signin')}>
          Sign In
        </MarginButton>
        <MarginButton type="primary" onClick={sendSentryError}>
          Test Sentry error (Do not click)
        </MarginButton>
      </Container>
    </>
  );
};

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 10rem;
`;

const MarginButton = styled(Button)`
  margin: 10px;
`;

const SearchOrder = styled(Input.Search)`
  width: 300px;
`;
