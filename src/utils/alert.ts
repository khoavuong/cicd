import { message } from 'antd';

export const alertError = (msg: string) => {
  message.error(msg);
};

export const alertSuccess = (msg: string) => {
  message.success(msg);
};

